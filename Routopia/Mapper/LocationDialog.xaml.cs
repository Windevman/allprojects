﻿namespace Mapper
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;

    /// <summary>
    /// Interaction logic for LocationDialog.xaml
    /// </summary>
    public partial class LocationDialog : Window
    {
        // Using a DependencyProperty as the backing store for ClipGroup.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ClipGroupProperty =
            DependencyProperty.Register("ClipGroup", typeof(Group), typeof(LocationDialog), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for Groups.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GroupsProperty =
            DependencyProperty.Register("Groups", typeof(ObservableCollection<Group>), typeof(LocationDialog), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for IntroGroup.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IntroGroupProperty =
            DependencyProperty.Register("IntroGroup", typeof(Group), typeof(LocationDialog), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for OutroGroup.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OutroGroupProperty =
            DependencyProperty.Register("OutroGroup", typeof(Group), typeof(LocationDialog), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for PostClipAdGroup.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PostClipAdGroupProperty =
            DependencyProperty.Register("PostClipAdGroup", typeof(Group), typeof(LocationDialog), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for PreClipAdGroup.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PreClipAdGroupProperty =
            DependencyProperty.Register("PreClipAdGroup", typeof(Group), typeof(LocationDialog), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for TriggerEast.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TriggerEastProperty =
            DependencyProperty.Register("TriggerEast", typeof(bool), typeof(LocationDialog), new PropertyMetadata(false));

        // Using a DependencyProperty as the backing store for TriggerNorthEast.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TriggerNorthEastProperty =
            DependencyProperty.Register("TriggerNorthEast", typeof(bool), typeof(LocationDialog), new PropertyMetadata(false));

        // Using a DependencyProperty as the backing store for TriggerNorth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TriggerNorthProperty =
            DependencyProperty.Register("TriggerNorth", typeof(bool), typeof(LocationDialog), new PropertyMetadata(false));

        // Using a DependencyProperty as the backing store for TriggerNorthWest.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TriggerNorthWestProperty =
            DependencyProperty.Register("TriggerNorthWest", typeof(bool), typeof(LocationDialog), new PropertyMetadata(false));

        // Using a DependencyProperty as the backing store for TriggerSouthEast.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TriggerSouthEastProperty =
            DependencyProperty.Register("TriggerSouthEast", typeof(bool), typeof(LocationDialog), new PropertyMetadata(false));

        // Using a DependencyProperty as the backing store for TriggerSouth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TriggerSouthProperty =
            DependencyProperty.Register("TriggerSouth", typeof(bool), typeof(LocationDialog), new PropertyMetadata(false));

        // Using a DependencyProperty as the backing store for TriggerSouthWest.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TriggerSouthWestProperty =
            DependencyProperty.Register("TriggerSouthWest", typeof(bool), typeof(LocationDialog), new PropertyMetadata(false));

        // Using a DependencyProperty as the backing store for TriggerWest.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TriggerWestProperty =
            DependencyProperty.Register("TriggerWest", typeof(bool), typeof(LocationDialog), new PropertyMetadata(false));

        // Using a DependencyProperty as the backing store for Description.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(LocationDialog), new PropertyMetadata(string.Empty));

        // Using a DependencyProperty as the backing store for East.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty EastProperty =
            DependencyProperty.Register("East", typeof(double), typeof(LocationDialog), new PropertyMetadata(0d));

        // Using a DependencyProperty as the backing store for Id.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty IdProperty =
            DependencyProperty.Register("Id", typeof(Guid), typeof(LocationDialog), new PropertyMetadata(Guid.Empty));

        // Using a DependencyProperty as the backing store for North.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty NorthProperty =
            DependencyProperty.Register("North", typeof(double), typeof(LocationDialog), new PropertyMetadata(0d));

        // Using a DependencyProperty as the backing store for South.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty SouthProperty =
            DependencyProperty.Register("South", typeof(double), typeof(LocationDialog), new PropertyMetadata(0d));

        // Using a DependencyProperty as the backing store for West.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty WestProperty =
            DependencyProperty.Register("West", typeof(double), typeof(LocationDialog), new PropertyMetadata(0d));

        // Using a DependencyProperty as the backing store for ZoneName.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty ZoneNameProperty =
            DependencyProperty.Register("ZoneName", typeof(string), typeof(LocationDialog), new PropertyMetadata(string.Empty));

        public Group ClipGroup
        {
            get { return (Group)GetValue(ClipGroupProperty); }
            set { SetValue(ClipGroupProperty, value); }
        }

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public double East
        {
            get { return (double)GetValue(EastProperty); }
            set { SetValue(EastProperty, value); }
        }

        public ObservableCollection<Group> Groups
        {
            get { return (ObservableCollection<Group>)GetValue(GroupsProperty); }
            set { SetValue(GroupsProperty, value); }
        }

        public Guid Id
        {
            get { return (Guid)GetValue(IdProperty); }
            set { SetValue(IdProperty, value); }
        }

        public Group IntroGroup
        {
            get { return (Group)GetValue(IntroGroupProperty); }
            set { SetValue(IntroGroupProperty, value); }
        }

        public double North
        {
            get { return (double)GetValue(NorthProperty); }
            set { SetValue(NorthProperty, value); }
        }

        public Group OutroGroup
        {
            get { return (Group)GetValue(OutroGroupProperty); }
            set { SetValue(OutroGroupProperty, value); }
        }

        public Group PostClipAdGroup
        {
            get { return (Group)GetValue(PostClipAdGroupProperty); }
            set { SetValue(PostClipAdGroupProperty, value); }
        }

        public Group PreClipAdGroup
        {
            get { return (Group)GetValue(PreClipAdGroupProperty); }
            set { SetValue(PreClipAdGroupProperty, value); }
        }

        public double South
        {
            get { return (double)GetValue(SouthProperty); }
            set { SetValue(SouthProperty, value); }
        }

        public bool TriggerEast
        {
            get { return (bool)GetValue(TriggerEastProperty); }
            set { SetValue(TriggerEastProperty, value); }
        }

        public bool TriggerNorth
        {
            get { return (bool)GetValue(TriggerNorthProperty); }
            set { SetValue(TriggerNorthProperty, value); }
        }

        public bool TriggerNorthEast
        {
            get { return (bool)GetValue(TriggerNorthEastProperty); }
            set { SetValue(TriggerNorthEastProperty, value); }
        }

        public bool TriggerNorthWest
        {
            get { return (bool)GetValue(TriggerNorthWestProperty); }
            set { SetValue(TriggerNorthWestProperty, value); }
        }

        public bool TriggerSouth
        {
            get { return (bool)GetValue(TriggerSouthProperty); }
            set { SetValue(TriggerSouthProperty, value); }
        }

        public bool TriggerSouthEast
        {
            get { return (bool)GetValue(TriggerSouthEastProperty); }
            set { SetValue(TriggerSouthEastProperty, value); }
        }

        public bool TriggerSouthWest
        {
            get { return (bool)GetValue(TriggerSouthWestProperty); }
            set { SetValue(TriggerSouthWestProperty, value); }
        }

        public bool TriggerWest
        {
            get { return (bool)GetValue(TriggerWestProperty); }
            set { SetValue(TriggerWestProperty, value); }
        }

        public double West
        {
            get { return (double)GetValue(WestProperty); }
            set { SetValue(WestProperty, value); }
        }

        public string ZoneName
        {
            get { return (string)GetValue(ZoneNameProperty); }
            set { SetValue(ZoneNameProperty, value); }
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void buttonClearClip_Click(object sender, RoutedEventArgs e)
        {
            this.ClipGroup = null;
        }

        private void buttonClearIntro_Click(object sender, RoutedEventArgs e)
        {
            this.IntroGroup = null;
        }

        private void buttonClearOutro_Click(object sender, RoutedEventArgs e)
        {
            this.OutroGroup = null;
        }

        private void buttonClearPostClipAd_Click(object sender, RoutedEventArgs e)
        {
            this.PostClipAdGroup = null;
        }

        private void buttonClearPreClipAd_Click(object sender, RoutedEventArgs e)
        {
            this.PreClipAdGroup = null;
        }

        private void buttonOk_Click(object sender, RoutedEventArgs e)
        {
            //originalZone.Name = this.ZoneName;
            //originalZone.Description = this.Description;
            //originalZone.North = this.North;
            //originalZone.East = this.East;
            //originalZone.South = this.South;
            //originalZone.West = this.West;
            //originalZone.TriggerNorth = this.TriggerNorth;
            //originalZone.TriggerNorthEast = this.TriggerNorthEast;
            //originalZone.TriggerEast = this.TriggerEast;
            //originalZone.TriggerSouthEast = this.TriggerSouthEast;
            //originalZone.TriggerSouth = this.TriggerSouth;
            //originalZone.TriggerSouthWest = this.TriggerSouthWest;
            //originalZone.TriggerWest = this.TriggerWest;
            //originalZone.TriggerNorthWest = this.TriggerNorthWest;

            //originalZone.IntroGroup = this.IntroGroup;
            //originalZone.PreClipAdGroup = this.PreClipAdGroup;
            //originalZone.ClipGroup = this.ClipGroup;
            //originalZone.PostClipAdGroup = this.PostClipAdGroup;
            //originalZone.OutroGroup = this.OutroGroup;

            //originalZone.Polygon.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Blue);
            //originalZone.Polygon.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Black);
            //originalZone.Polygon.StrokeThickness = 2;

            //this.DialogResult = true;
            //this.Close();
        }
    }
}