﻿namespace Mapper
{
    using System;
    using System.Windows;

    public class AudioClip : DependencyObject
    {
        // Using a DependencyProperty as the backing store for AudioType.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty AudioTypeProperty =
            DependencyProperty.Register("AudioType", typeof(int), typeof(AudioClip), new PropertyMetadata(0, OnAudioTypeChanged));

        // Using a DependencyProperty as the backing store for Description.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(AudioClip), new PropertyMetadata(string.Empty, OnDescriptionChanged));

        // Using a DependencyProperty as the backing store for Id.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty IdProperty =
            DependencyProperty.Register("Id", typeof(Guid), typeof(AudioClip), new PropertyMetadata(Guid.Empty, OnIdChanged));

        // Using a DependencyProperty as the backing store for IsDirty.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty IsDirtyProperty =
            DependencyProperty.Register("IsDirty", typeof(bool), typeof(AudioClip), new PropertyMetadata(false));

        // Using a DependencyProperty as the backing store for Name.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty NameProperty =
            DependencyProperty.Register("Name", typeof(string), typeof(AudioClip), new PropertyMetadata(string.Empty, OnNameChanged));

        // Using a DependencyProperty as the backing store for URL.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty URLProperty =
            DependencyProperty.Register("URL", typeof(string), typeof(AudioClip), new PropertyMetadata(string.Empty, OnURLChanged));

        public AudioClip()
        {
            this.Id = Guid.NewGuid();
        }

        public int AudioType
        {
            get { return (int)GetValue(AudioTypeProperty); }
            set { SetValue(AudioTypeProperty, value); }
        }

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public Guid Id
        {
            get { return (Guid)GetValue(IdProperty); }
            set { SetValue(IdProperty, value); }
        }

        public bool IsDirty
        {
            get { return (bool)GetValue(IsDirtyProperty); }
            set { SetValue(IsDirtyProperty, value); }
        }

        public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        public string URL
        {
            get { return (string)GetValue(URLProperty); }
            set { SetValue(URLProperty, value); }
        }

        private static void OnAudioTypeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            (obj as AudioClip).IsDirty = true;
        }

        private static void OnDescriptionChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            (obj as AudioClip).IsDirty = true;
        }

        private static void OnIdChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            (obj as AudioClip).IsDirty = true;
        }

        private static void OnNameChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            (obj as AudioClip).IsDirty = true;
        }

        private static void OnURLChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            (obj as AudioClip).IsDirty = true;
        }
    }
}