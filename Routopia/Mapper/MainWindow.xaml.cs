﻿namespace Mapper
{
    using Mapper.RoutopiaWS;

    ////using Routopia.Shared;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Json;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Media;
    using maps = Microsoft.Maps.MapControl.WPF;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Using a DependencyProperty as the backing store for ActiveGroup.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ActiveGroupProperty =
            DependencyProperty.Register("ActiveGroup", typeof(Group), typeof(MainWindow), new PropertyMetadata(null, new PropertyChangedCallback(OnActiveGroupChanged)));

        // Using a DependencyProperty as the backing store for AudioClips.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AudioClipsProperty =
            DependencyProperty.Register("AudioClips", typeof(ObservableCollection<AudioClip>), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for AudioClipsToDelete.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AudioClipsToDeleteProperty =
            DependencyProperty.Register("AudioClipsToDelete", typeof(ObservableCollection<AudioClip>), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for AudioTypes.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AudioTypesProperty =
            DependencyProperty.Register("AudioTypes", typeof(ObservableCollection<AudioType>), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for GroupFilterText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GroupFilterTextProperty =
            DependencyProperty.Register("GroupFilterText", typeof(string), typeof(MainWindow), new PropertyMetadata(string.Empty, new PropertyChangedCallback(OnGroupFilterTextChanged)));

        // Using a DependencyProperty as the backing store for GroupsCV.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GroupsCVProperty =
            DependencyProperty.Register("GroupsCV", typeof(CollectionView), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for Groups.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GroupsProperty =
            DependencyProperty.Register("Groups", typeof(ObservableCollection<Group>), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for GroupsToDelete.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GroupsToDeleteProperty =
            DependencyProperty.Register("GroupsToDelete", typeof(ObservableCollection<Group>), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for InsightsToDelete.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty InsightsToDeleteProperty =
            DependencyProperty.Register("InsightsToDelete", typeof(ObservableCollection<InsightMarker>), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for Pins.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PinsProperty =
            DependencyProperty.Register("Pins", typeof(ObservableCollection<DraggablePin>), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for Regions.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RegionsProperty =
            DependencyProperty.Register("Regions", typeof(ObservableCollection<Region>), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for SelectedAudioClip.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedAudioClipProperty =
            DependencyProperty.Register("SelectedAudioClip", typeof(AudioClip), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for SelectedChildGroup.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedChildGroupProperty =
            DependencyProperty.Register("SelectedChildGroup", typeof(Group), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for SelectedGroupAudioClip.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedGroupAudioClipProperty =
            DependencyProperty.Register("SelectedGroupAudioClip", typeof(AudioClip), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for SelectedGroup.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedGroupProperty =
            DependencyProperty.Register("SelectedGroup", typeof(Group), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for SelectedInsightMarker.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedInsightMarkerProperty =
            DependencyProperty.Register("SelectedInsightMarker", typeof(InsightMarker), typeof(MainWindow), new PropertyMetadata(null, OnSelectedInsightMarkerChanged));

        // Using a DependencyProperty as the backing store for SelectedParentGroup.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedParentGroupProperty =
            DependencyProperty.Register("SelectedParentGroup", typeof(Group), typeof(MainWindow), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for SelectedTriggerMarker.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedTriggerMarkerProperty =
            DependencyProperty.Register("SelectedTriggerMarker", typeof(TriggerMarker), typeof(MainWindow), new PropertyMetadata(null, OnSelectedTriggerMarkerChanged));

        /// <summary>
        /// backing store for AddAudioClipCommand
        /// </summary>
        private RelayCommand storageForAddAudioClipCommand;

        /// <summary>
        /// backing store for AddChildGroupCommand
        /// </summary>
        private RelayCommand storageForAddChildGroupCommand;

        /// <summary>
        /// backing store for AddGroupCommand
        /// </summary>
        private RelayCommand storageForAddGroupCommand;

        /// <summary>
        /// backing store for AddParentGroupCommand
        /// </summary>
        private RelayCommand storageForAddParentGroupCommand;

        /// <summary>
        /// backing store for DeleteAudioClipCommand
        /// </summary>
        private RelayCommand storageForDeleteAudioClipCommand;

        /// <summary>
        /// backing store for DeleteChildGroupCommand
        /// </summary>
        private RelayCommand storageForDeleteChildGroupCommand;

        /// <summary>
        /// backing store for DeleteGroupCommand
        /// </summary>
        private RelayCommand storageForDeleteGroupCommand;

        /// <summary>
        /// backing store for DeleteLocationCommand
        /// </summary>
        private RelayCommand storageForDeleteLocationCommand;

        /// <summary>
        /// backing store for DeleteParentGroupCommand
        /// </summary>
        private RelayCommand storageForDeleteParentGroupCommand;

        /// <summary>
        /// backing store for ModifyAudioClipCommand
        /// </summary>
        private RelayCommand storageForModifyAudioClipCommand;

        /// <summary>
        /// backing store for ModifyGroupCommand
        /// </summary>
        private RelayCommand storageForModifyGroupCommand;

        /// <summary>
        /// backing store for ModifyLocationCommand
        /// </summary>
        private RelayCommand storageForModifyLocationCommand;

        /// <summary>
        /// backing store for NewAudioClipCommand
        /// </summary>
        private RelayCommand storageForNewAudioClipCommand;

        /// <summary>
        /// backing store for NewGroupCommand
        /// </summary>
        private RelayCommand storageForNewGroupCommand;

        /// <summary>
        /// backing store for NewLocationCommand
        /// </summary>
        private RelayCommand storageForNewLocationCommand;

        /// <summary>
        /// backing store for RefreshGroupsCommand
        /// </summary>
        private RelayCommand storageForRefreshGroupsCommand;

        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = this;

            this.AudioClips = new ObservableCollection<AudioClip>();
            this.Groups = new ObservableCollection<Group>();
            this.InsightsToDelete = new ObservableCollection<InsightMarker>();
            this.AudioTypes = new ObservableCollection<AudioType>();
            this.GroupsToDelete = new ObservableCollection<Group>();
            this.AudioClipsToDelete = new ObservableCollection<AudioClip>();
            this.Regions = new ObservableCollection<Region>();

            this.AudioClips.CollectionChanged += AudioClips_CollectionChanged;

            this.GroupsCV = CollectionViewSource.GetDefaultView(this.Groups) as CollectionView;
            this.GroupsCV.SortDescriptions.Add(new System.ComponentModel.SortDescription("Name", System.ComponentModel.ListSortDirection.Ascending));

            this.GroupsCV.Filter = new Predicate<object>((obj) =>
            {
                if (obj is Group group)
                {
                    if (this.GroupFilterText == null || this.GroupFilterText == string.Empty)
                    {
                        return true;
                    }

                    if (group.Description.ToLower().Contains(this.GroupFilterText.ToLower()) == true ||
                        group.Name.ToLower().Contains(this.GroupFilterText.ToLower()) == true)
                    {
                        return true;
                    }
                }

                return false;
            });

            AlphaGrouper grouper = new AlphaGrouper();
            this.GroupsCV.GroupDescriptions.Add(new PropertyGroupDescription("Name", grouper));

            if (this.GroupsCV is ListCollectionView lcv)
            {
                lcv.IsLiveFiltering = true;
                lcv.IsLiveGrouping = true;
                lcv.IsLiveSorting = true;

                lcv.LiveFilteringProperties.Add("Name");
                lcv.LiveGroupingProperties.Add("Name");
                lcv.LiveSortingProperties.Add("Name");
            }

            this.Groups.CollectionChanged += Groups_CollectionChanged;

            this.Pins = new ObservableCollection<DraggablePin>();
        }

        public Group ActiveGroup
        {
            get { return (Group)GetValue(ActiveGroupProperty); }
            set { SetValue(ActiveGroupProperty, value); }
        }

        /// <summary>
        /// Gets the AddAudioClip command
        /// </summary>
        public ICommand AddAudioClipCommand
        {
            get
            {
                if (this.storageForAddAudioClipCommand == null)
                {
                    this.storageForAddAudioClipCommand = new RelayCommand(param => this.AddAudioClip(), param => this.CanAddAudioClip());
                }

                return this.storageForAddAudioClipCommand;
            }
        }

        /// <summary>
        /// Gets the AddChildGroup command
        /// </summary>
        public ICommand AddChildGroupCommand
        {
            get
            {
                if (this.storageForAddChildGroupCommand == null)
                {
                    this.storageForAddChildGroupCommand = new RelayCommand(param => this.AddChildGroup(), param => this.CanAddChildGroup());
                }

                return this.storageForAddChildGroupCommand;
            }
        }

        /// <summary>
        /// Gets the AddGroup command
        /// </summary>
        public ICommand AddGroupCommand
        {
            get
            {
                if (this.storageForAddGroupCommand == null)
                {
                    this.storageForAddGroupCommand = new RelayCommand(param => this.AddGroup(), param => this.CanAddGroup());
                }

                return this.storageForAddGroupCommand;
            }
        }

        /// <summary>
        /// Gets the AddParentGroup command
        /// </summary>
        public ICommand AddParentGroupCommand
        {
            get
            {
                if (this.storageForAddParentGroupCommand == null)
                {
                    this.storageForAddParentGroupCommand = new RelayCommand(param => this.AddParentGroup(), param => this.CanAddParentGroup());
                }

                return this.storageForAddParentGroupCommand;
            }
        }

        public ObservableCollection<AudioClip> AudioClipsToDelete
        {
            get { return (ObservableCollection<AudioClip>)GetValue(AudioClipsToDeleteProperty); }
            set { SetValue(AudioClipsToDeleteProperty, value); }
        }

        public ObservableCollection<AudioType> AudioTypes
        {
            get { return (ObservableCollection<AudioType>)GetValue(AudioTypesProperty); }
            set { SetValue(AudioTypesProperty, value); }
        }

        /// <summary>
        /// Gets the DeleteAudioClip command
        /// </summary>
        public ICommand DeleteAudioClipCommand
        {
            get
            {
                if (this.storageForDeleteAudioClipCommand == null)
                {
                    this.storageForDeleteAudioClipCommand = new RelayCommand(param => this.DeleteAudioClip(), param => this.CanDeleteAudioClip());
                }

                return this.storageForDeleteAudioClipCommand;
            }
        }

        /// <summary>
        /// Gets the DeleteChildGroup command
        /// </summary>
        public ICommand DeleteChildGroupCommand
        {
            get
            {
                if (this.storageForDeleteChildGroupCommand == null)
                {
                    this.storageForDeleteChildGroupCommand = new RelayCommand(param => this.DeleteChildGroup(), param => this.CanDeleteChildGroup());
                }

                return this.storageForDeleteChildGroupCommand;
            }
        }

        /// <summary>
        /// Gets the DeleteGroup command
        /// </summary>
        public ICommand DeleteGroupCommand
        {
            get
            {
                if (this.storageForDeleteGroupCommand == null)
                {
                    this.storageForDeleteGroupCommand = new RelayCommand(param => this.DeleteGroup(), param => this.CanDeleteGroup());
                }

                return this.storageForDeleteGroupCommand;
            }
        }

        /// <summary>
        /// Gets the DeleteParentGroup command
        /// </summary>
        public ICommand DeleteParentGroupCommand
        {
            get
            {
                if (this.storageForDeleteParentGroupCommand == null)
                {
                    this.storageForDeleteParentGroupCommand = new RelayCommand(param => this.DeleteParentGroup(), param => this.CanDeleteParentGroup());
                }

                return this.storageForDeleteParentGroupCommand;
            }
        }

        public string GroupFilterText
        {
            get { return (string)GetValue(GroupFilterTextProperty); }
            set { SetValue(GroupFilterTextProperty, value); }
        }

        public ObservableCollection<Group> Groups
        {
            get { return (ObservableCollection<Group>)GetValue(GroupsProperty); }
            set { SetValue(GroupsProperty, value); }
        }

        public CollectionView GroupsCV
        {
            get { return (CollectionView)GetValue(GroupsCVProperty); }
            set { SetValue(GroupsCVProperty, value); }
        }

        public ObservableCollection<Group> GroupsToDelete
        {
            get { return (ObservableCollection<Group>)GetValue(GroupsToDeleteProperty); }
            set { SetValue(GroupsToDeleteProperty, value); }
        }

        public ObservableCollection<InsightMarker> InsightsToDelete
        {
            get { return (ObservableCollection<InsightMarker>)GetValue(InsightsToDeleteProperty); }
            set { SetValue(InsightsToDeleteProperty, value); }
        }

        public ObservableCollection<DraggablePin> Pins
        {
            get { return (ObservableCollection<DraggablePin>)GetValue(PinsProperty); }
            set { SetValue(PinsProperty, value); }
        }

        ////        return this.storageForNewLocationCommand;
        ////    }
        ////}
        /// <summary>
        /// Gets the RefreshGroups command
        /// </summary>
        public ICommand RefreshGroupsCommand
        {
            get
            {
                if (this.storageForRefreshGroupsCommand == null)
                {
                    this.storageForRefreshGroupsCommand = new RelayCommand(param => this.RefreshGroups(), param => this.CanRefreshGroups());
                }

                return this.storageForRefreshGroupsCommand;
            }
        }

        public ObservableCollection<Region> Regions
        {
            get { return (ObservableCollection<Region>)GetValue(RegionsProperty); }
            set { SetValue(RegionsProperty, value); }
        }

        /////// <summary>
        /////// Gets the NewLocation command
        /////// </summary>
        ////public ICommand NewLocationCommand
        ////{
        ////    get
        ////    {
        ////        if (this.storageForNewLocationCommand == null)
        ////        {
        ////            this.storageForNewLocationCommand = new RelayCommand(async (param) => await this.NewLocation(), param => this.CanNewLocation());
        ////        }
        public AudioClip SelectedAudioClip
        {
            get { return (AudioClip)GetValue(SelectedAudioClipProperty); }
            set { SetValue(SelectedAudioClipProperty, value); }
        }

        ////        return this.storageForNewGroupCommand;
        ////    }
        ////}
        public Group SelectedChildGroup
        {
            get { return (Group)GetValue(SelectedChildGroupProperty); }
            set { SetValue(SelectedChildGroupProperty, value); }
        }

        /////// <summary>
        /////// Gets the NewGroup command
        /////// </summary>
        ////public ICommand NewGroupCommand
        ////{
        ////    get
        ////    {
        ////        if (this.storageForNewGroupCommand == null)
        ////        {
        ////            this.storageForNewGroupCommand = new RelayCommand(param => this.NewGroup(), param => this.CanNewGroup());
        ////        }
        public Group SelectedGroup
        {
            get { return (Group)GetValue(SelectedGroupProperty); }
            set { SetValue(SelectedGroupProperty, value); }
        }

        public AudioClip SelectedGroupAudioClip
        {
            get { return (AudioClip)GetValue(SelectedGroupAudioClipProperty); }
            set { SetValue(SelectedGroupAudioClipProperty, value); }
        }

        /////// <summary>
        /////// Gets the ModifyAudioClip command
        /////// </summary>
        ////public ICommand ModifyAudioClipCommand
        ////{
        ////    get
        ////    {
        ////        if (this.storageForModifyAudioClipCommand == null)
        ////        {
        ////            this.storageForModifyAudioClipCommand = new RelayCommand(async (param) => await this.ModifyAudioClip(), param => this.CanModifyAudioClip());
        ////        }

        ////        return this.storageForModifyAudioClipCommand;
        ////    }
        ////}

        ///// <summary>
        ///// Gets the ModifyGroup command
        ///// </summary>
        //public ICommand ModifyGroupCommand
        //{
        //    get
        //    {
        //        if (this.storageForModifyGroupCommand == null)
        //        {
        //            this.storageForModifyGroupCommand = new RelayCommand(param => this.ModifyGroup(), param => this.CanModifyGroup());
        //        }

        //        return this.storageForModifyGroupCommand;
        //    }
        //}

        /////// <summary>
        /////// Gets the NewAudioClip command
        /////// </summary>
        ////public ICommand NewAudioClipCommand
        ////{
        ////    get
        ////    {
        ////        if (this.storageForNewAudioClipCommand == null)
        ////        {
        ////            this.storageForNewAudioClipCommand = new RelayCommand(async (param) => await this.NewAudioClip(), param => this.CanNewAudioClip());
        ////        }

        ////        return this.storageForNewAudioClipCommand;
        ////    }
        ////}
        public InsightMarker SelectedInsightMarker
        {
            get { return (InsightMarker)GetValue(SelectedInsightMarkerProperty); }
            set { SetValue(SelectedInsightMarkerProperty, value); }
        }

        public Group SelectedParentGroup
        {
            get { return (Group)GetValue(SelectedParentGroupProperty); }
            set { SetValue(SelectedParentGroupProperty, value); }
        }

        public TriggerMarker SelectedTriggerMarker
        {
            get { return (TriggerMarker)GetValue(SelectedTriggerMarkerProperty); }
            set { SetValue(SelectedTriggerMarkerProperty, value); }
        }

        private ObservableCollection<AudioClip> AudioClips
        {
            get { return (ObservableCollection<AudioClip>)GetValue(AudioClipsProperty); }
            set { SetValue(AudioClipsProperty, value); }
        }

        static public void OnSelectedInsightMarkerChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MainWindow mainWindow = obj as MainWindow;

            if (e.OldValue is InsightMarker marker)
            {
                marker.IsSelected = false;

                foreach (TriggerMarker trigger in marker.TriggerList)
                {
                    mainWindow.myMap.Children.Remove(trigger);
                }
            }

            if (e.NewValue is InsightMarker marker2)
            {
                marker2.IsSelected = true;

                foreach (TriggerMarker trigger in marker2.TriggerList)
                {
                    mainWindow.myMap.Children.Add(trigger);
                }
            }
        }

        static public void OnSelectedTriggerMarkerChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MainWindow mainWindow = obj as MainWindow;

            if (e.OldValue is TriggerMarker marker)
            {
                marker.IsSelected = false;
            }

            if (e.NewValue is TriggerMarker marker2)
            {
                marker2.IsSelected = true;
            }
        }

        /// <summary>
        /// Execute the AddAudioClip command
        /// </summary>
        public void AddAudioClip()
        {
            this.ActiveGroup.AudioClips.Add(this.SelectedAudioClip);

            this.LinkAudio();
        }

        /// <summary>
        /// Execute the AddChildGroup command
        /// </summary>
        public void AddChildGroup()
        {
            this.ActiveGroup.ChildGroups.Add(this.SelectedGroup);
            this.SelectedGroup.ParentGroups.Add(this.ActiveGroup);
            this.LinkAudio();
        }

        /// <summary>
        /// Execute the AddGroup command
        /// </summary>
        /// <returns>A work in progress</returns>
        public void AddGroup()
        {
            Group group = new Group()
            {
                Name = "<new group>",
                Id = Guid.NewGuid()
            };

            this.Groups.Add(group);

            this.ActiveGroup = group;
            this.SelectedGroup = group;
        }

        /// <summary>
        /// Execute the AddParentGroup command
        /// </summary>
        public void AddParentGroup()
        {
            this.ActiveGroup.ParentGroups.Add(this.SelectedGroup);
            this.SelectedGroup.ChildGroups.Add(this.ActiveGroup);
            this.LinkAudio();
        }

        /// <summary>
        /// Can the AddAudioClip command execute
        /// </summary>
        /// <returns>True if the command can execute, otherwise false</returns>
        public bool CanAddAudioClip()
        {
            if (this.ActiveGroup != null && this.SelectedAudioClip != null && this.ActiveGroup.AudioClips.Contains(this.SelectedAudioClip) == false)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Can the AddChildGroup command execute
        /// </summary>
        /// <returns>True if the command can execute, otherwise false</returns>
        public bool CanAddChildGroup()
        {
            if (this.SelectedGroup == null)
            {
                return false;
            }

            if (this.ActiveGroup == null)
            {
                return false;
            }

            if (this.SelectedGroup == this.ActiveGroup)
            {
                return false;
            }

            if (this.SelectedGroup.Type != this.ActiveGroup.Type)
            {
                return false;
            }

            if (this.SelectedGroup.IsDescendentOf(this.ActiveGroup) == true)
            {
                return false;
            }

            if (this.ActiveGroup.IsDescendentOf(this.SelectedGroup) == true)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Can the AddGroup command execute
        /// </summary>
        /// <returns>True if the command can execute, otherwise false</returns>
        public bool CanAddGroup()
        {
            return true;
        }

        /// <summary>
        /// Can the AddParentGroup command execute
        /// </summary>
        /// <returns>True if the command can execute, otherwise false</returns>
        public bool CanAddParentGroup()
        {
            if (this.SelectedGroup == null)
            {
                return false;
            }

            if (this.ActiveGroup == null)
            {
                return false;
            }

            if (this.SelectedGroup == this.ActiveGroup)
            {
                return false;
            }

            if (this.SelectedGroup.Type != this.ActiveGroup.Type)
            {
                return false;
            }

            if (this.SelectedGroup.IsDescendentOf(this.ActiveGroup) == true)
            {
                return false;
            }

            if (this.ActiveGroup.IsDescendentOf(this.SelectedGroup) == true)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Can the DeleteAudioClip command execute
        /// </summary>
        /// <returns>True if the command can execute, otherwise false</returns>
        public bool CanDeleteAudioClip()
        {
            if (this.ActiveGroup != null && this.SelectedGroupAudioClip != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Can the DeleteChildGroup command execute
        /// </summary>
        /// <returns>True if the command can execute, otherwise false</returns>
        public bool CanDeleteChildGroup()
        {
            if (this.SelectedChildGroup != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Can the DeleteGroup command execute
        /// </summary>
        /// <returns>True if the command can execute, otherwise false</returns>
        public bool CanDeleteGroup()
        {
            if (this.SelectedGroup != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Can the DeleteParentGroup command execute
        /// </summary>
        /// <returns>True if the command can execute, otherwise false</returns>
        public bool CanDeleteParentGroup()
        {
            if (this.SelectedParentGroup != null)
            {
                return true;
            }

            return false;
        }

        /////// <summary>
        /////// Can the ModifyAudioClip command execute
        /////// </summary>
        /////// <returns>True if the command can execute, otherwise false</returns>
        ////public bool CanModifyAudioClip()
        ////{
        ////    return false;
        ////    ////if (this.SelectedAudioClip != null)
        ////    ////{
        ////    ////    return true;
        ////    ////}

        ////    ////return false;
        ////}

        /////// <summary>
        /////// Can the ModifyGroup command execute
        /////// </summary>
        /////// <returns>True if the command can execute, otherwise false</returns>
        ////public bool CanModifyGroup()
        ////{
        ////    if (this.SelectedGroup != null)
        ////    {
        ////        return true;
        ////    }

        ////    return false;
        ////}

        /// <summary>
        /// Can the NewAudioClip command execute
        /// </summary>
        /// <returns>True if the command can execute, otherwise false</returns>
        public bool CanNewAudioClip()
        {
            return false;
        }

        /// <summary>
        /// Can the NewGroup command execute
        /// </summary>
        /// <returns>True if the command can execute, otherwise false</returns>
        public bool CanNewGroup()
        {
            // TODO: determine when NewGroup can execute
            return true;
        }

        /// <summary>
        /// Can the NewLocation command execute
        /// </summary>
        /// <returns>True if the command can execute, otherwise false</returns>
        public bool CanNewLocation()
        {
            // TODO: determine when NewLocation can execute
            return false;
        }

        /// <summary>
        /// Can the RefreshGroups command execute
        /// </summary>
        /// <returns>True if the command can execute, otherwise false</returns>
        public bool CanRefreshGroups()
        {
            // TODO: determine when RefreshGroups can execute
            return true;
        }

        /// <summary>
        /// Execute the DeleteAudioClip command
        /// </summary>
        public void DeleteAudioClip()
        {
            this.ActiveGroup.AudioClips.Remove(this.SelectedGroupAudioClip);

            this.SelectedGroupAudioClip = null;

            this.LinkAudio();
        }

        /// <summary>
        /// Execute the DeleteChildGroup command
        /// </summary>
        public void DeleteChildGroup()
        {
            Group parent = this.ActiveGroup;
            Group child = this.SelectedChildGroup;

            parent.ChildGroups.Remove(child);
            child.ParentGroups.Remove(parent);
        }

        /// <summary>
        /// Execute the DeleteGroup command
        /// </summary>
        public void DeleteGroup()
        {
            if (this.SelectedGroup != null)
            {
                Group group = this.SelectedGroup;
                this.SelectedGroup = null;

                this.GroupsToDelete.Add(group);
                this.Groups.Remove(group);

                foreach (var parent in group.ParentGroups)
                {
                    parent.ChildGroups.Remove(group);
                }

                foreach (var child in group.ChildGroups)
                {
                    child.ParentGroups.Remove(group);
                }

                foreach (DraggablePin pin in this.Pins)
                {
                    if (pin is InsightMarker insight)
                    {
                        if (insight.IntroGroup == group)
                        {
                            insight.IntroGroup = null;
                        }

                        if (insight.AudioGroup == group)
                        {
                            insight.AudioGroup = null;
                        }

                        if (insight.OutroGroup == group)
                        {
                            insight.OutroGroup = null;
                        }

                        foreach (TriggerMarker trigger in insight.TriggerList)
                        {
                            if (trigger.PreAdGroup == group)
                            {
                                trigger.PreAdGroup = null;
                            }

                            if (trigger.PostAdGroup == group)
                            {
                                trigger.PostAdGroup = null;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Execute the DeleteParentGroup command
        /// </summary>
        public void DeleteParentGroup()
        {
            Group parent = this.SelectedParentGroup;
            Group child = this.ActiveGroup;

            child.ParentGroups.Remove(parent);
            parent.ChildGroups.Remove(child);
        }

        public ItemsControl GetSelectedTreeViewItemParent(TreeViewItem item)
        {
            DependencyObject parent = VisualTreeHelper.GetParent(item);
            while (!(parent is TreeViewItem || parent is TreeView))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }

            return parent as ItemsControl;
        }

        /// <summary>
        /// Execute the ModifyAudioClip command
        /// </summary>
        /// <returns>A work in progress</returns>
        public async Task ModifyAudioClip()
        {
            // TODO: execute the ModifyAudioClip command
            // eliminates a warning - This can be removed if any await method calls are used.
            await Task.Run(() =>
            {
            });
        }

        /////// <summary>
        /////// Execute the ModifyGroup command
        /////// </summary>
        ////public void ModifyGroup()
        ////{
        ////    if (this.SelectedGroup != null)
        ////    {
        ////        GroupDialog dialog = new GroupDialog(this.SelectedGroup);

        ////        if (dialog.ShowDialog() == true)
        ////        {
        ////            this.SaveGroup(this.SelectedGroup);
        ////        }
        ////    }
        ////}

        /// <summary>
        /// Execute the NewAudioClip command
        /// </summary>
        /// <returns>A work in progress</returns>
        public async Task NewAudioClip()
        {
            // TODO: execute the NewAudioClip command
            // eliminates a warning - This can be removed if any await method calls are used.
            await Task.Run(() =>
            {
            });
        }

        /// <summary>
        /// Execute the NewGroup command
        /// </summary>
        public void NewGroup()
        {
            Group group = new Group()
            {
                Name = "New Group",
                Id = Guid.NewGuid()
            };

            GroupDialog dialog = new GroupDialog(group);

            if (dialog.ShowDialog() == true)
            {
                this.SaveGroup(group);
                this.Groups.Add(group);
                this.SelectedGroup = group;
            }
        }

        /// <summary>
        /// Execute the NewLocation command
        /// </summary>
        /// <returns>A work in progress</returns>
        public async Task NewLocation()
        {
            // TODO: execute the NewLocation command
            // eliminates a warning - This can be removed if any await method calls are used.
            await Task.Run(() =>
            {
            });
        }

        /// <summary>
        /// Execute the RefreshGroups command
        /// </summary>
        public void RefreshGroups()
        {
            this.GroupsCV.Refresh();
        }

        private static void OnActiveGroupChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MainWindow window = obj as MainWindow;

            window.SelectedParentGroup = null;
            window.SelectedChildGroup = null;
        }

        private static void OnGroupFilterTextChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MainWindow window = obj as MainWindow;

            window.GroupsCV.Refresh();
        }

        private void AudioClips_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                foreach (AudioClip clip in e.OldItems)
                {
                    this.AudioClipsToDelete.Add(clip);

                    foreach (Group g in this.Groups)
                    {
                        if (g.AudioClips.Contains(clip))
                        {
                            g.AudioClips.Remove(clip);
                        }
                    }
                }
            }
        }

        private void buttonBrowseAudio_Click(object sender, RoutedEventArgs e)
        {
            GroupSelectorDialog dialog = new GroupSelectorDialog(this.Groups, this.SelectedInsightMarker.AudioGroup);

            if (dialog.ShowDialog() == true)
            {
                this.SelectedInsightMarker.AudioGroup = dialog.SelectedGroup;
            }
        }

        private void buttonBrowseIntro_Click(object sender, RoutedEventArgs e)
        {
            GroupSelectorDialog dialog = new GroupSelectorDialog(this.Groups, this.SelectedInsightMarker.IntroGroup);

            if (dialog.ShowDialog() == true)
            {
                this.SelectedInsightMarker.IntroGroup = dialog.SelectedGroup;
            }
        }

        private void buttonBrowseOutro_Click(object sender, RoutedEventArgs e)
        {
            GroupSelectorDialog dialog = new GroupSelectorDialog(this.Groups, this.SelectedInsightMarker.OutroGroup);

            if (dialog.ShowDialog() == true)
            {
                this.SelectedInsightMarker.OutroGroup = dialog.SelectedGroup;
            }
        }

        private void buttonBrowsePostAd_Click(object sender, RoutedEventArgs e)
        {
            GroupSelectorDialog dialog = new GroupSelectorDialog(this.Groups, this.SelectedTriggerMarker.PostAdGroup);

            if (dialog.ShowDialog() == true)
            {
                this.SelectedTriggerMarker.PostAdGroup = dialog.SelectedGroup;
            }
        }

        private void buttonBrowsePreAd_Click(object sender, RoutedEventArgs e)
        {
            GroupSelectorDialog dialog = new GroupSelectorDialog(this.Groups, this.SelectedTriggerMarker.PreAdGroup);

            if (dialog.ShowDialog() == true)
            {
                this.SelectedTriggerMarker.PreAdGroup = dialog.SelectedGroup;
            }
        }

        private void buttonClearAudio_Click(object sender, RoutedEventArgs e)
        {
            if (this.SelectedInsightMarker != null)
            {
                this.SelectedInsightMarker.AudioGroup = null;
            }
        }

        private void buttonClearIntro_Click(object sender, RoutedEventArgs e)
        {
            if (this.SelectedInsightMarker != null)
            {
                this.SelectedInsightMarker.IntroGroup = null;
            }
        }

        private void buttonClearOutro_Click(object sender, RoutedEventArgs e)
        {
            if (this.SelectedInsightMarker != null)
            {
                this.SelectedInsightMarker.OutroGroup = null;
            }
        }

        private void buttonClearPostAd_Click(object sender, RoutedEventArgs e)
        {
            if (this.SelectedTriggerMarker != null)
            {
                this.SelectedTriggerMarker.PostAdGroup = null;
            }
        }

        private void buttonClearPreAd_Click(object sender, RoutedEventArgs e)
        {
            if (this.SelectedTriggerMarker != null)
            {
                this.SelectedTriggerMarker.PreAdGroup = null;
            }
        }

        private void buttonCloseInsightSettings_Click(object sender, RoutedEventArgs e)
        {
            this.SelectedInsightMarker = null;
            this.SelectedTriggerMarker = null;
        }

        private void buttonCloseTriggerSettings_Click(object sender, RoutedEventArgs e)
        {
            this.SelectedTriggerMarker = null;
        }

        private void buttonDeleteInsight_Click(object sender, RoutedEventArgs e)
        {
            if ((sender as Button).Tag is InsightMarker marker)
            {
                this.SelectedInsightMarker = null;
                this.SelectedTriggerMarker = null;

                if (this.myMap.Children.Contains(marker) == true)
                {
                    this.myMap.Children.Remove(marker);
                }

                this.InsightsToDelete.Add(marker);
            }
        }

        private void buttonDeleteLink_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (sender as Button).Tag as TreeViewItem;

            if (item != null)
            {
                ItemsControl parent = ItemsControl.ItemsControlFromItemContainer(item);

                if (parent != null)
                {
                    Group group = parent.DataContext as Group;
                    AudioClip clip = item.DataContext as AudioClip;

                    group.AudioClips.Remove(clip);

                    using (RoutopiaWS.RoutopiaServiceClient client = new RoutopiaWS.RoutopiaServiceClient("BasicHttpBinding_IRoutopiaService"))
                    {
                        DetachAudioRequest request = new DetachAudioRequest()
                        {
                            AudioGroupId = group.Id,
                            AudioId = clip.Id
                        };

                        DetachAudioResponse response = client.DetachAudio(request);
                    }
                }
            }
        }

        private void buttonDeleteTrigger_Click(object sender, RoutedEventArgs e)
        {
            if ((sender as Button).Tag is TriggerMarker marker)
            {
                this.SelectedTriggerMarker = null;
                marker.ParentInsight.TriggerList.Remove(marker);
                if (this.myMap.Children.Contains(marker) == true)
                {
                    this.myMap.Children.Remove(marker);
                }
            }
        }

        private void buttonLoad_Click(object sender, RoutedEventArgs e)
        {
        }

        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            using (RoutopiaWS.RoutopiaServiceClient client = new RoutopiaWS.RoutopiaServiceClient())
            {
                // Save the insights
                List<Routopia.Shared.Insight> insights = new List<Routopia.Shared.Insight>();

                foreach (DraggablePin pin in this.Pins)
                {
                    if (pin is InsightMarker marker && marker.IsDirty == true)
                    {
                        Routopia.Shared.Insight insight = new Routopia.Shared.Insight()
                        {
                            AudioId = marker.AudioGroup?.Id,
                            Description = marker.Description,
                            Id = marker.Id,
                            IntroGroupId = marker.IntroGroup?.Id,
                            Name = marker.Title,
                            OutroGroupId = marker.OutroGroup?.Id,
                            Latitude = marker.Location.Latitude,
                            Longitude = marker.Location.Longitude
                        };

                        List<Routopia.Shared.Trigger> triggers = new List<Routopia.Shared.Trigger>();

                        foreach (TriggerMarker tmarker in marker.TriggerList)
                        {
                            Routopia.Shared.Trigger trigger = new Routopia.Shared.Trigger()
                            {
                                Latitude = tmarker.Location.Latitude,
                                Longitude = tmarker.Location.Longitude,
                                PostAdGroupId = tmarker.PostAdGroup?.Id,
                                PreAdGroupId = tmarker.PreAdGroup?.Id,
                                TriggerEast = tmarker.TriggerEast,
                                TriggerNorth = tmarker.TriggerNorth,
                                TriggerNorthEast = tmarker.TriggerNorthEast,
                                TriggerNorthWest = tmarker.TriggerNorthWest,
                                TriggerSouth = tmarker.TriggerSouth,
                                TriggerSouthEast = tmarker.TriggerSouthEast,
                                TriggerSouthWest = tmarker.TriggerSouthWest,
                                TriggerWest = tmarker.TriggerWest,
                                RoadType = (Routopia.Shared.RoadTypes)tmarker.RoadType,
                                RoadNumber = tmarker.RoadNumber,
                                RoadState = tmarker.RoadState,
                                TriggerDistance = tmarker.TriggerDistance
                            };

                            triggers.Add(trigger);

                            tmarker.IsDirty = false;
                        }

                        insight.Triggers = triggers.ToArray();

                        insights.Add(insight);

                        marker.IsDirty = false;
                    }
                }

                if (insights.Count > 0)
                {
                    Guid guid = WriteCache(insights, Routopia.Shared.DataCacheTypes.Temporary);

                    if (guid != Guid.Empty)
                    {
                        RoutopiaWS.WriteInsightsRequest request = new RoutopiaWS.WriteInsightsRequest()
                        {
                            CacheId = guid
                        };

                        RoutopiaWS.WriteInsightsResponse response = client.WriteInsights(request);
                    }
                }

                // Save the audio groups
                List<Routopia.Shared.AudioGroup> audioGroups = new List<Routopia.Shared.AudioGroup>();

                var modifiedGroups = (from g in this.Groups
                                      where g.IsDirty == true
                                      select g).ToArray();

                foreach (var modifiedGroup in modifiedGroups)
                {
                    Routopia.Shared.AudioGroup audioGroup = new Routopia.Shared.AudioGroup()
                    {
                        Id = modifiedGroup.Id,
                        Name = modifiedGroup.Name,
                        Description = modifiedGroup.Description,
                        Type = modifiedGroup.Type
                    };

                    audioGroups.Add(audioGroup);

                    modifiedGroup.IsDirty = false;
                }

                if (audioGroups.Count > 0)
                {
                    Guid guid = WriteCache(audioGroups, Routopia.Shared.DataCacheTypes.Temporary);

                    if (guid != Guid.Empty)
                    {
                        RoutopiaWS.WriteAudioGroupsRequest request = new RoutopiaWS.WriteAudioGroupsRequest()
                        {
                            CacheId = guid
                        };

                        RoutopiaWS.WriteAudioGroupsResponse response = client.WriteAudioGroups(request);
                    }
                }

                // Save the audio clips
                var modifiedAudioClips = (from a in this.AudioClips
                                          where a.IsDirty == true
                                          select a).ToList();

                List<Routopia.Shared.AudioClip> audioClips = new List<Routopia.Shared.AudioClip>();

                foreach (AudioClip clip in modifiedAudioClips)
                {
                    Routopia.Shared.AudioClip audio = new Routopia.Shared.AudioClip()
                    {
                        Id = clip.Id,
                        Description = clip.Description,
                        Name = clip.Name,
                        Type = clip.AudioType,
                        URL = clip.URL
                    };

                    audioClips.Add(audio);

                    clip.IsDirty = false;
                }

                if (audioClips.Count > 0)
                {
                    Guid guid = WriteCache(audioClips, Routopia.Shared.DataCacheTypes.Temporary);

                    if (guid != Guid.Empty)
                    {
                        RoutopiaWS.WriteAudioClipsRequest request = new RoutopiaWS.WriteAudioClipsRequest()
                        {
                            CacheId = guid
                        };

                        RoutopiaWS.WriteAudioClipsResponse response = client.WriteAudioClips(request);
                    }
                }

                // Save the group links
                List<Routopia.Shared.GroupLink> groupLinks = new List<Routopia.Shared.GroupLink>();
                List<Routopia.Shared.AudioGroupLink> audioGroupLinks = new List<Routopia.Shared.AudioGroupLink>();

                foreach (var modifiedGroup in modifiedGroups)
                {
                    foreach (var childGroup in modifiedGroup.ChildGroups)
                    {
                        Routopia.Shared.GroupLink link = new Routopia.Shared.GroupLink()
                        {
                            ParentGroupId = modifiedGroup.Id,
                            ChildGroupId = childGroup.Id
                        };

                        groupLinks.Add(link);
                    }

                    foreach (var audioClip in modifiedGroup.AudioClips)
                    {
                        Routopia.Shared.AudioGroupLink link = new Routopia.Shared.AudioGroupLink()
                        {
                            AudioClipId = audioClip.Id,
                            AudioGroupId = modifiedGroup.Id
                        };

                        audioGroupLinks.Add(link);
                    }
                }

                if (groupLinks.Count > 0)
                {
                    Guid guid = WriteCache(groupLinks, Routopia.Shared.DataCacheTypes.Temporary);

                    if (guid != Guid.Empty)
                    {
                        RoutopiaWS.WriteGroupLinksRequest request = new WriteGroupLinksRequest()
                        {
                            CacheId = guid
                        };

                        RoutopiaWS.WriteGroupLinksResponse response = client.WriteGroupLinks(request);
                    }
                }

                if (audioGroupLinks.Count > 0)
                {
                    Guid guid = WriteCache(audioGroupLinks, Routopia.Shared.DataCacheTypes.Temporary);

                    if (guid != Guid.Empty)
                    {
                        RoutopiaWS.WriteAudioGroupLinksRequest request = new WriteAudioGroupLinksRequest()
                        {
                            CacheId = guid
                        };

                        RoutopiaWS.WriteAudioGroupLinksResponse response = client.WriteAudioGroupLinks(request);
                    }
                }

                // Delete the insights
                foreach (InsightMarker marker in this.InsightsToDelete.ToArray())
                {
                    RoutopiaWS.DeleteInsightRequest request = new RoutopiaWS.DeleteInsightRequest()
                    {
                        InsightId = marker.Id
                    };

                    RoutopiaWS.DeleteInsightResponse response = client.DeleteInsight(request);

                    if (response.Result == false)
                    {
                        MessageBox.Show(response.ErrorMessage);
                    }
                    else
                    {
                        this.InsightsToDelete.Clear();
                    }
                }

                // Delete the groups
                if (this.GroupsToDelete.Count() > 0)
                {
                    var items = from g in this.GroupsToDelete
                                select g.Id;

                    RoutopiaWS.DeleteAudioGroupRequest request = new DeleteAudioGroupRequest()
                    {
                        AudioGroupIdList = items.ToArray()
                    };

                    RoutopiaWS.DeleteAudioGroupResponse response = client.DeleteAudioGroups(request);

                    if (response.Result == false)
                    {
                        MessageBox.Show(response.ErrorMessage);
                    }
                    else
                    {
                        this.GroupsToDelete.Clear();
                    }
                }

                // Delete the audio clips
                if (this.AudioClipsToDelete.Count() > 0)
                {
                    var items = from a in this.AudioClipsToDelete
                                select a.Id;

                    RoutopiaWS.DeleteAudioClipsRequest request = new DeleteAudioClipsRequest()
                    {
                        AudioClipIdList = items.ToArray()
                    };

                    RoutopiaWS.DeleteAudioClipsResponse response = client.DeleteAudioClips(request);

                    if (response.Result == false)
                    {
                        MessageBox.Show(response.ErrorMessage);
                    }
                    else
                    {
                        this.AudioClipsToDelete.Clear();
                    }
                }
            }
        }

        private void buttonTestAudio_Click(object sender, RoutedEventArgs e)
        {
            //using (RoutopiaWS.RoutopiaServiceClient client = new RoutopiaWS.RoutopiaServiceClient("httpStreaming"))
            //{
            //    RoutopiaWS.AudioClip clip = new RoutopiaWS.AudioClip()
            //    {
            //        Name = "Test 1",
            //        Description = "Test 1 Description",
            //        LastUpdated = DateTime.Now
            //    };

            //    byte[] data = null;

            //    using (System.IO.FileStream fs = new System.IO.FileStream(@"c:\junk\hello.wav", System.IO.FileMode.Open))
            //    {
            //        data = new byte[fs.Length];

            //        fs.Read(data, 0, data.Length);

            //        clip.Data = data;
            //        clip.DataLength = data.Length;
            //    }

            //    RoutopiaWS.AudioClipRequest request = new RoutopiaWS.AudioClipRequest()
            //    {
            //        Clip = clip
            //    };

            //    RoutopiaWS.AudioClipResponse response = client.SaveAudioClip(request);

            //    if (response.Result == true)
            //    {
            //        clip.Id = response.Clip.Id;
            //    }
            //    else
            //    {
            //        MessageBox.Show(response.ErrorMessage);
            //    }

            //    client.Close();
            //}
        }

        private void DataGrid_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        private void DataGrid_Drop(object sender, DragEventArgs e)
        {
            List<AudioClip> clips = new List<AudioClip>();

            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            foreach (string file in files)
            {
                byte[] data = null;

                using (System.IO.FileStream fs = new FileStream(file, FileMode.Open))
                {
                    data = new byte[fs.Length];

                    fs.Read(data, 0, data.Length);
                }

                AudioClip item = new AudioClip()
                {
                    Name = System.IO.Path.GetFileNameWithoutExtension(file),
                    Description = file,
                    AudioType = 0,
                };

                AudioClips.Add(item);
                clips.Add(item);
            }

            SaveAudioFiles(clips);
        }

        private void DataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //this.ModifyLocation();
        }

        private AudioClip[] GetChildAudioLinks(Group group)
        {
            List<AudioClip> clips = new List<AudioClip>();

            clips.AddRange(group.AudioClips);

            foreach (Group g in group.ChildGroups)
            {
                clips.AddRange(GetChildAudioLinks(g));
            }

            return clips.ToArray();
        }

        private void Groups_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //this.GroupsCV.Refresh();
        }

        //private void buttonModifyGroup_Click(object sender, RoutedEventArgs e)
        //{
        //}
        //private ObservableCollection<AudioClip> LinkAudioClips(Group g, AudioGroupLink[] audioGroupLinks, ObservableCollection<Group> searchedGroups)
        //{
        //    ObservableCollection<AudioClip> results = new ObservableCollection<AudioClip>();

        //    var links = from link in audioGroupLinks
        //                where link.AudioGroupId == g.Id
        //                select link;

        //    foreach (AudioGroupLink link in links)
        //    {
        //        AudioClip clip = this.AudioClips.FirstOrDefault(c => c.Id == link.AudioClipId);

        //        if (clip != null && results.Contains(clip) == false)
        //        {
        //            results.Add(clip);
        //        }
        //    }

        //    foreach (Group g2 in g.ChildGroups)
        //    {
        //        if (searchedGroups.Contains(g2) == false)
        //        {
        //            searchedGroups.Add(g2);
        //            results.AddRange(this.LinkAudioClips(g2, audioGroupLinks, searchedGroups));
        //        }
        //    }

        //    return results;
        //}

        private void LinkAudio()
        {
            try
            {
                foreach (Group g in Groups)
                {
                    g.InheritedAudioClips.Clear();

                    AudioClip[] clips = this.GetChildAudioLinks(g);
                    var distinct = clips.Distinct();

                    g.InheritedAudioClips.AddRange(distinct);

                    foreach (AudioClip clip in g.AudioClips)
                    {
                        g.InheritedAudioClips.Remove(clip);
                    }
                }
            }
            catch (System.Exception ex)
            {
            }
        }

        private void LinkGroups(Routopia.Shared.GroupLink[] groupLinks, Routopia.Shared.AudioGroupLink[] audioGroupLinks)
        {
            foreach (Group g in Groups)
            {
                g.ParentGroups.Clear();
                g.ChildGroups.Clear();

                var clips = from c in audioGroupLinks
                            where c.AudioGroupId == g.Id
                            select c;

                foreach (var clip in clips)
                {
                    AudioClip ac = this.AudioClips.FirstOrDefault(c => c.Id == clip.AudioClipId);

                    if (ac != null)
                    {
                        g.AudioClips.Add(ac);
                    }
                }

                // find all the parents of object
                var groups = from gl in groupLinks
                             where gl.ChildGroupId == g.Id
                             select gl;

                foreach (Routopia.Shared.GroupLink gl in groups)
                {
                    Group group = Groups.FirstOrDefault(gr => gr.Id == gl.ParentGroupId);

                    if (group != null)
                    {
                        g.ParentGroups.Add(group);
                    }
                }

                // find all the children of object
                groups = from gl in groupLinks
                         where gl.ParentGroupId == g.Id
                         select gl;

                foreach (Routopia.Shared.GroupLink gl in groups)
                {
                    Group group = Groups.FirstOrDefault(gr => gr.Id == gl.ChildGroupId);

                    if (group != null)
                    {
                        g.ChildGroups.Add(group);
                    }
                }

                g.IsDirty = false;
            }

            this.LinkAudio();
        }

        private void listGroupChildren_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.ActiveGroup = this.SelectedChildGroup;
        }

        private void listGroups_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.ActiveGroup = this.SelectedGroup;
        }

        private void listParentGroups_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.ActiveGroup = this.SelectedParentGroup;
        }

        private void LoadData()
        {
            this.AudioClips.Clear();
            this.myMap.Children.Clear();
            this.AudioTypes.Clear();
            this.Regions.Clear();

            Routopia.Shared.AudioGroupLink[] audioGroupLinks = null;
            Routopia.Shared.GroupLink[] groupLinks = null;

            using (RoutopiaWS.RoutopiaServiceClient client = new RoutopiaWS.RoutopiaServiceClient("BasicHttpBinding_IRoutopiaService"))
            {
                RoutopiaWS.ReadAudioGroupsRequest request3 = new RoutopiaWS.ReadAudioGroupsRequest();
                RoutopiaWS.ReadAudioGroupsResponse response3 = client.ReadAudioGroups(request3);

                if (response3.Result == true)
                {
                    Routopia.Shared.AudioGroup[] groups = this.ReadCache<Routopia.Shared.AudioGroup[]>(response3.CacheId);

                    foreach (Routopia.Shared.AudioGroup group in groups)
                    {
                        Group temp = new Group()
                        {
                            Id = group.Id,
                            Name = group.Name,
                            Description = group.Description,
                            Type = group.Type
                        };

                        this.Groups.Add(temp);
                    }
                }

                RoutopiaWS.ReadInsightsRequest request = new RoutopiaWS.ReadInsightsRequest();
                RoutopiaWS.ReadInsightsResponse response = client.ReadInsights(request);

                if (response.Result == true)
                {
                    Routopia.Shared.Insight[] insights = this.ReadCache<Routopia.Shared.Insight[]>(response.CacheId);

                    foreach (var insight in insights)
                    {
                        InsightMarker marker = new InsightMarker(this.myMap)
                        {
                            AudioGroup = this.Groups.FirstOrDefault(g => g.Id == insight.AudioId),
                            Description = insight.Description,
                            Id = insight.Id,
                            IntroGroup = this.Groups.FirstOrDefault(g => g.Id == insight.IntroGroupId),
                            Location = new maps.Location(insight.Latitude, insight.Longitude),
                            Title = insight.Name,
                            OutroGroup = this.Groups.FirstOrDefault(g => g.Id == insight.OutroGroupId)
                        };

                        Pins.Add(marker);

                        this.myMap.Children.Add(marker);

                        marker.TriggerList = new ObservableCollection<TriggerMarker>();

                        foreach (Routopia.Shared.Trigger t in insight.Triggers)
                        {
                            TriggerMarker tmarker = new TriggerMarker(this.myMap, marker)
                            {
                                Id = t.Id,
                                Location = new maps.Location(t.Latitude, t.Longitude),
                                PostAdGroup = this.Groups.FirstOrDefault(g => g.Id == t.PostAdGroupId),
                                PreAdGroup = this.Groups.FirstOrDefault(g => g.Id == t.PreAdGroupId),
                                TriggerNorth = t.TriggerNorth,
                                TriggerNorthEast = t.TriggerNorthEast,
                                TriggerEast = t.TriggerEast,
                                TriggerSouthEast = t.TriggerSouthEast,
                                TriggerSouth = t.TriggerSouth,
                                TriggerSouthWest = t.TriggerSouthWest,
                                TriggerWest = t.TriggerWest,
                                TriggerNorthWest = t.TriggerNorthWest,
                                RoadType = (int)t.RoadType,
                                RoadNumber = t.RoadNumber,
                                RoadState = t.RoadState,
                                TriggerDistance = t.TriggerDistance
                            };

                            tmarker.IsDirty = false;

                            marker.TriggerList.Add(tmarker);
                        }

                        marker.IsDirty = false;
                    }
                }

                RoutopiaWS.ReadAudioClipsRequest request2 = new RoutopiaWS.ReadAudioClipsRequest();
                RoutopiaWS.ReadAudioClipsResponse response2 = client.ReadAudioClips(request2);

                if (response2.Result == true)
                {
                    Routopia.Shared.AudioClip[] clips = this.ReadCache<Routopia.Shared.AudioClip[]>(response2.CacheId);

                    foreach (Routopia.Shared.AudioClip clip in clips)
                    {
                        AudioClip temp = new AudioClip()
                        {
                            AudioType = clip.Type,
                            Description = clip.Description,
                            Id = clip.Id,
                            Name = clip.Name,
                            URL = clip.URL
                        };

                        temp.IsDirty = false;

                        this.AudioClips.Add(temp);
                    }
                }

                RoutopiaWS.ReadGroupLinksRequest request4 = new RoutopiaWS.ReadGroupLinksRequest();
                RoutopiaWS.ReadGroupLinksResponse response4 = client.ReadGroupLinks(request4);

                if (response4.Result == true)
                {
                    groupLinks = this.ReadCache<Routopia.Shared.GroupLink[]>(response4.CacheId);
                }

                RoutopiaWS.ReadAudioGroupLinksRequest request5 = new RoutopiaWS.ReadAudioGroupLinksRequest();
                RoutopiaWS.ReadAudioGroupLinksResponse response5 = client.ReadAudioGroupLinks(request5);

                if (response5.Result == true)
                {
                    audioGroupLinks = this.ReadCache<Routopia.Shared.AudioGroupLink[]>(response5.CacheId);
                }

                RoutopiaWS.ReadAudioTypesRequest request6 = new RoutopiaWS.ReadAudioTypesRequest();
                RoutopiaWS.ReadAudioTypesResponse response6 = client.ReadAudioTypes(request6);

                if (response6.Result == true)
                {
                    Routopia.Shared.AudioType[] types = this.ReadCache<Routopia.Shared.AudioType[]>(response6.CacheId);

                    foreach (Routopia.Shared.AudioType type in types)
                    {
                        AudioType newType = new AudioType()
                        {
                            Id = type.Id,
                            Name = type.Name
                        };

                        this.AudioTypes.Add(newType);
                    }
                }

                RoutopiaWS.ReadRegionsRequest request7 = new ReadRegionsRequest();
                RoutopiaWS.ReadRegionsResponse response7 = client.ReadRegions(request7);

                if (response7.Result == true)
                {
                    Routopia.Shared.Region[] regions = this.ReadCache<Routopia.Shared.Region[]>(response7.CacheId);

                    foreach (Routopia.Shared.Region region in regions)
                    {
                        Region newRegion = new Region()
                        {
                            Id = region.Id,
                            CountryName = region.CountryName,
                            RegionName = region.RegionName
                        };

                        this.Regions.Add(newRegion);
                    }
                }

                ////Task<RoutopiaWS.AudioClipResponse[]> audioTask = client.GetAudioClipsAsync();
                ////RoutopiaWS.AudioClipResponse[] clips = await audioTask;

                //using (MemoryStream stream = new MemoryStream(response.))
                //{
                //    BinaryFormatter formatter = new BinaryFormatter();
                //    formatter.Deserialize(stream)
                //}

                //foreach (RoutopiaWS.AudioClipResponse response in clips)
                //{
                //    if (response.Result == true)
                //    {
                //        AudioClip clip = new AudioClip()
                //        {
                //            Data = response.Clip.Data,
                //            DataLength = response.Clip.DataLength,
                //            Description = response.Clip.Description,
                //            Id = response.Clip.Id,
                //            LastUpdated = response.Clip.LastUpdated,
                //            Name = response.Clip.Name
                //        };

                //        this.AudioClips.Add(clip);
                //    }
                //}
            }

            this.LinkGroups(groupLinks, audioGroupLinks);
        }

        private void Map_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                maps.Map map = sender as maps.Map;
                Point p = e.GetPosition(map);
                maps.Location location = map.ViewportPointToLocation(p);

                if (this.SelectedInsightMarker != null)
                {
                    TriggerMarker marker = new TriggerMarker(map, this.SelectedInsightMarker)
                    {
                        Location = location,
                        IsDirty = true
                    };

                    myMap.Children.Add(marker);

                    this.Pins.Add(marker);

                    this.SelectedInsightMarker.TriggerList.Add(marker);

                    this.SelectedTriggerMarker = marker;
                }
                else
                {
                    InsightMarker marker = new InsightMarker(map)
                    {
                        Location = location,
                        IsDirty = true
                    };

                    myMap.Children.Add(marker);

                    this.Pins.Add(marker);

                    this.SelectedInsightMarker = marker;
                }
            }
            else
            {
            }
        }

        private T ReadCache<T>(Guid cacheId)
        {
            using (RoutopiaWS.RoutopiaServiceClient client = new RoutopiaWS.RoutopiaServiceClient("BasicHttpBinding_IRoutopiaService"))
            {
                List<byte> data = new List<byte>();

                RoutopiaWS.ReadCacheRequest request = new RoutopiaWS.ReadCacheRequest()
                {
                    CacheId = cacheId,
                    Sequence = 0
                };

                RoutopiaWS.ReadCacheResponse response = null;

                do
                {
                    response = client.ReadCache(request);

                    data.AddRange(response.Data);

                    request.Sequence++;
                }
                while (response.Complete == false);

                RoutopiaWS.DeleteCacheRequest deleteRequest = new RoutopiaWS.DeleteCacheRequest()
                {
                    CacheId = cacheId
                };

                client.DeleteCache(deleteRequest);

                byte[] buffer = data.ToArray();

                using (MemoryStream stream = new MemoryStream(buffer))
                {
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
                    return (T)serializer.ReadObject(stream);
                }
            }
        }

        private void SaveAudio(AudioClip clip)
        {
            List<AudioClip> clips = new List<AudioClip>()
            {
                clip
            };

            this.SaveAudioFiles(clips);
        }

        private void SaveAudioFiles(List<AudioClip> clips)
        {
            ////using (RoutopiaWS.RoutopiaServiceClient client = new RoutopiaWS.RoutopiaServiceClient())
            ////{
            ////    List<Audio> audioClips = new List<Audio>();

            ////    foreach (AudioClip clip in clips)
            ////    {
            ////        Audio audio = new Audio()
            ////        {
            ////            Id = clip.Id != Guid.Empty ? clip.Id : Guid.NewGuid(),
            ////            DataLength = clip.DataLength,
            ////            Description = clip.Description,
            ////            LastUpdated = DateTime.Now,
            ////            Name = clip.Name,
            ////            Type = clip.AudioType
            ////        };

            ////        audioClips.Add(audio);
            ////    }

            ////    if (clips.Count > 0)
            ////    {
            ////        Guid guid = WriteCache(audioClips, DataCacheTypes.Temporary);

            ////        if (guid != Guid.Empty)
            ////        {
            ////            RoutopiaWS.WriteAudioRequest request = new RoutopiaWS.WriteAudioRequest()
            ////            {
            ////                CacheId = guid
            ////            };

            ////            RoutopiaWS.WriteAudioResponse response = client.WriteAudio(request);

            ////            foreach (AudioClip clip in clips)
            ////            {
            ////                this.WriteAudioData(clip);
            ////            }
            ////        }
            ////    }
            ////}
        }

        private void SaveGroup(Group group)
        {
            List<Group> groups = new List<Group>()
            {
                group
            };

            this.SaveGroups(groups);
        }

        private void SaveGroups(List<Group> groups)
        {
        }

        private void Test_Click(object sender, RoutedEventArgs e)
        {
        }

        private void treeAudioGroups_DragEnter(object sender, DragEventArgs e)
        {
        }

        private void treeAudioGroups_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (e.NewValue is Group)
            {
                this.SelectedGroup = e.NewValue as Group;
            }
            else
            {
                this.SelectedGroup = null;
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.LoadData();
        }

        private Guid WriteAudioData(AudioClip clip)
        {
            ////int size = clip.Data.Length;
            ////int offset = 0;
            ////int sequence = 0;
            ////Guid cacheId = Guid.Empty;

            ////using (RoutopiaWS.RoutopiaServiceClient client = new RoutopiaWS.RoutopiaServiceClient("BasicHttpBinding_IBlueCopper"))
            ////{
            ////    byte[] buffer = new byte[32 * 1024];

            ////    while (size > 0)
            ////    {
            ////        if (size > 32 * 1024)
            ////        {
            ////            Array.Copy(clip.Data, offset, buffer, 0, buffer.Length);

            ////            RoutopiaWS.WriteCacheRequest request = new RoutopiaWS.WriteCacheRequest()
            ////            {
            ////                Sequence = sequence,
            ////                LastSegment = false,
            ////                Data = buffer,
            ////                CacheId = cacheId,
            ////                Type = DataCacheTypes.AudioData,
            ////                ForcedGuid = clip.Id
            ////            };

            ////            RoutopiaWS.WriteCacheResponse response = client.WriteCache(request);

            ////            cacheId = response.CacheId;

            ////            size -= buffer.Length;
            ////            offset += buffer.Length;
            ////        }
            ////        else
            ////        {
            ////            buffer = new byte[size];

            ////            Array.Copy(clip.Data, offset, buffer, 0, buffer.Length);

            ////            RoutopiaWS.WriteCacheRequest request = new RoutopiaWS.WriteCacheRequest()
            ////            {
            ////                Sequence = sequence,
            ////                LastSegment = true,
            ////                Data = buffer,
            ////                CacheId = cacheId,
            ////                Type = DataCacheTypes.AudioData,
            ////                ForcedGuid = clip.Id
            ////            };

            ////            RoutopiaWS.WriteCacheResponse response = client.WriteCache(request);

            ////            cacheId = response.CacheId;

            ////            size -= buffer.Length;
            ////            offset += buffer.Length;
            ////        }

            ////        sequence++;
            ////    }
            ////}

            ////return cacheId;

            throw new NotImplementedException();
        }

        private Guid WriteCache<T>(List<T> list, Routopia.Shared.DataCacheTypes type)
        {
            byte[] data = null;

            using (MemoryStream stream = new MemoryStream())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T[]));

                serializer.WriteObject(stream, list.ToArray());
                data = stream.ToArray();
            }

            int size = data.Length;
            int offset = 0;
            int sequence = 0;
            Guid cacheId = Guid.Empty;

            using (RoutopiaWS.RoutopiaServiceClient client = new RoutopiaWS.RoutopiaServiceClient("BasicHttpBinding_IRoutopiaService"))
            {
                byte[] buffer = new byte[32 * 1024];

                while (size > 0)
                {
                    if (size > 32 * 1024)
                    {
                        Array.Copy(data, offset, buffer, 0, buffer.Length);

                        RoutopiaWS.WriteCacheRequest request = new RoutopiaWS.WriteCacheRequest()
                        {
                            Sequence = sequence,
                            LastSegment = false,
                            Data = buffer,
                            CacheId = cacheId,
                            Type = type
                        };

                        RoutopiaWS.WriteCacheResponse response = client.WriteCache(request);

                        cacheId = response.CacheId;

                        size -= buffer.Length;
                        offset += buffer.Length;
                    }
                    else
                    {
                        buffer = new byte[size];

                        Array.Copy(data, offset, buffer, 0, buffer.Length);

                        RoutopiaWS.WriteCacheRequest request = new RoutopiaWS.WriteCacheRequest()
                        {
                            Sequence = sequence,
                            LastSegment = true,
                            Data = buffer,
                            CacheId = cacheId,
                            Type = type
                        };

                        RoutopiaWS.WriteCacheResponse response = client.WriteCache(request);

                        cacheId = response.CacheId;

                        size -= buffer.Length;
                        offset += buffer.Length;
                    }

                    sequence++;
                }
            }

            return cacheId;
        }
    }
}