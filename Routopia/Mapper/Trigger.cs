﻿using System;
using System.Windows;

namespace Mapper
{
    public class Trigger : DependencyObject
    {
        // Using a DependencyProperty as the backing store for Id.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty IdProperty =
            DependencyProperty.Register("Id", typeof(Guid), typeof(Trigger), new PropertyMetadata(Guid.Empty));


        public Guid Id
        {
            get { return (Guid)GetValue(IdProperty); }
            set { SetValue(IdProperty, value); }
        }

    }
}