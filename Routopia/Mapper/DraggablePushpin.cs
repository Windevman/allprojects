﻿using Microsoft.Maps.MapControl.WPF;
using System.Windows;
using System.Windows.Input;

namespace Mapper
{
    public class DraggablePin : Pushpin
    {
        // Using a DependencyProperty as the backing store for IsDirty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsDirtyProperty =
            DependencyProperty.Register("IsDirty", typeof(bool), typeof(DraggablePin), new PropertyMetadata(false, new PropertyChangedCallback(OnIsDirtyChanged)));

        // Using a DependencyProperty as the backing store for IsLocked.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsLockedProperty =
            DependencyProperty.Register("IsLocked", typeof(bool), typeof(DraggablePin), new PropertyMetadata(false));

        private Location _center;

        private Map _map;

        private bool isDragging = false;

        private Point offset = new Point();

        public DraggablePin(Map map)
        {
            _map = map;
            this.DataContext = this;
            this.IsLocked = true;
        }

        public bool IsDirty
        {
            get { return (bool)GetValue(IsDirtyProperty); }
            set { SetValue(IsDirtyProperty, value); }
        }

        public bool IsLocked
        {
            get { return (bool)GetValue(IsLockedProperty); }
            set { SetValue(IsLockedProperty, value); }
        }

        public static void OnIsDirtyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (obj is DraggablePin pin)
            {
                pin.OnIsDirtyChangedOverride(obj, args);
            }
        }

        public virtual void OnIsDirtyChangedOverride(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            MainWindow mainWindow = _map.Tag as MainWindow;

            if (mainWindow != null && this is InsightMarker marker)
            {
                if (mainWindow.SelectedInsightMarker != marker)
                {
                    mainWindow.SelectedInsightMarker = marker;
                    mainWindow.SelectedTriggerMarker = null;
                }
            }

            if (mainWindow != null && this is TriggerMarker marker2)
            {
                mainWindow.SelectedTriggerMarker = marker2;
            }

            if (this.IsLocked == false)
            {
                if (_map != null)
                {
                    Point mousePos = e.GetPosition(_map);
                    Point locationPos = _map.LocationToViewportPoint(this.Location);

                    offset = new Point(mousePos.X - locationPos.X, mousePos.Y - locationPos.Y);

                    _center = _map.Center;

                    _map.ViewChangeOnFrame += _map_ViewChangeOnFrame;
                    _map.MouseUp += ParentMap_MouseLeftButtonUp;
                    _map.MouseMove += ParentMap_MouseMove;
                }

                // Enable Dragging
                this.isDragging = true;
            }

            base.OnMouseLeftButtonDown(e);
        }

        protected override void OnMouseRightButtonUp(MouseButtonEventArgs e)
        {
            MainWindow mainWindow = _map.Tag as MainWindow;

            if (mainWindow != null && this is InsightMarker marker)
            {
                mainWindow.SelectedInsightMarker = marker;
            }

            ////InsightSettings settings = new InsightSettings();

            ////settings.Show();

            ////Popup popup = new Popup();

            ////popup.Width = 300;
            ////popup.Height = 300;
            ////popup.StaysOpen = false;
            ////popup.Placement = PlacementMode.Mouse;
            ////popup.Child = new InsightMarkerSettings()
            ////{
            ////    HorizontalContentAlignment = HorizontalAlignment.Stretch,
            ////    VerticalContentAlignment = VerticalAlignment.Stretch,
            ////    Width = 300,
            ////    Height = 300
            ////};

            ////popup.IsOpen = true;

            base.OnMouseRightButtonUp(e);
        }

        private void _map_ViewChangeOnFrame(object sender, MapEventArgs e)
        {
            if (isDragging)
            {
                _map.Center = _center;
            }
        }

        private void ParentMap_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            // Left Mouse Button released, stop dragging the Pushpin
            if (_map != null)
            {
                _map.ViewChangeOnFrame -= _map_ViewChangeOnFrame;
                _map.MouseUp -= ParentMap_MouseLeftButtonUp;
                _map.MouseMove -= ParentMap_MouseMove;
            }

            this.isDragging = false;
        }

        private void ParentMap_MouseMove(object sender, MouseEventArgs e)
        {
            var map = sender as Microsoft.Maps.MapControl.WPF.Map;
            // Check if the user is currently dragging the Pushpin
            if (this.isDragging)
            {
                // If so, the Move the Pushpin to where the Mouse is.
                var mouseMapPosition = e.GetPosition(map);
                mouseMapPosition.X -= offset.X;
                mouseMapPosition.Y -= offset.Y;
                var mouseGeocode = map.ViewportPointToLocation(mouseMapPosition);
                this.Location = mouseGeocode;
                this.IsDirty = true;
            }
        }
    }
}