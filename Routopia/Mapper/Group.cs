﻿namespace Mapper
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;

    public class Group : DependencyObject
    {
        // Using a DependencyProperty as the backing store for AudioClips.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AudioClipsProperty =
            DependencyProperty.Register("AudioClips", typeof(ObservableCollection<AudioClip>), typeof(Group), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for ChildGroups.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ChildGroupsProperty =
            DependencyProperty.Register("ChildGroups", typeof(ObservableCollection<Group>), typeof(Group), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for Description.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(Group), new PropertyMetadata(string.Empty, new PropertyChangedCallback(OnDescriptionChanged)));

        // Using a DependencyProperty as the backing store for Id.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IdProperty =
            DependencyProperty.Register("Id", typeof(Guid), typeof(Group), new PropertyMetadata(Guid.Empty));

        // Using a DependencyProperty as the backing store for InheritedAudioClips.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty InheritedAudioClipsProperty =
            DependencyProperty.Register("InheritedAudioClips", typeof(ObservableCollection<AudioClip>), typeof(AudioClip), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for IsDirty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsDirtyProperty =
            DependencyProperty.Register("IsDirty", typeof(bool), typeof(Group), new PropertyMetadata(false));

        // Using a DependencyProperty as the backing store for LastUpdated.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LastUpdatedProperty =
            DependencyProperty.Register("LastUpdated", typeof(DateTime), typeof(Group), new PropertyMetadata(DateTime.MinValue));

        // Using a DependencyProperty as the backing store for Name.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register("Name", typeof(string), typeof(Group), new PropertyMetadata(string.Empty, new PropertyChangedCallback(OnNameChanged)));

        // Using a DependencyProperty as the backing store for ParentGroups.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ParentGroupsProperty =
            DependencyProperty.Register("ParentGroups", typeof(ObservableCollection<Group>), typeof(Group), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for Type.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TypeProperty =
            DependencyProperty.Register("Type", typeof(int), typeof(Group), new PropertyMetadata(0, new PropertyChangedCallback(OnTypeChanged)));

        public Group()
        {
            this.AudioClips = new ObservableCollection<AudioClip>();
            this.ChildGroups = new ObservableCollection<Group>();
            this.ParentGroups = new ObservableCollection<Group>();
            this.InheritedAudioClips = new ObservableCollection<AudioClip>();

            this.ChildGroups.CollectionChanged += this.ChildGroups_CollectionChanged;
            this.ParentGroups.CollectionChanged += this.ParentGroups_CollectionChanged;
            this.AudioClips.CollectionChanged += AudioClips_CollectionChanged;
        }

        private void AudioClips_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.IsDirty = true;
        }

        public ObservableCollection<AudioClip> AudioClips
        {
            get { return (ObservableCollection<AudioClip>)GetValue(AudioClipsProperty); }
            set { SetValue(AudioClipsProperty, value); }
        }

        public ObservableCollection<Group> ChildGroups
        {
            get { return (ObservableCollection<Group>)GetValue(ChildGroupsProperty); }
            set { SetValue(ChildGroupsProperty, value); }
        }

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public Guid Id
        {
            get { return (Guid)GetValue(IdProperty); }
            set { SetValue(IdProperty, value); }
        }

        public ObservableCollection<AudioClip> InheritedAudioClips
        {
            get { return (ObservableCollection<AudioClip>)GetValue(InheritedAudioClipsProperty); }
            set { SetValue(InheritedAudioClipsProperty, value); }
        }

        public bool IsDirty
        {
            get { return (bool)GetValue(IsDirtyProperty); }
            set { SetValue(IsDirtyProperty, value); }
        }

        public DateTime LastUpdated
        {
            get { return (DateTime)GetValue(LastUpdatedProperty); }
            set { SetValue(LastUpdatedProperty, value); }
        }

        public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        public ObservableCollection<Group> ParentGroups
        {
            get { return (ObservableCollection<Group>)GetValue(ParentGroupsProperty); }
            set { SetValue(ParentGroupsProperty, value); }
        }

        public int Type
        {
            get { return (int)GetValue(TypeProperty); }
            set { SetValue(TypeProperty, value); }
        }

        public bool IsDescendentOf(Group group)
        {
            if (group.ChildGroups.Contains(this) == true)
            {
                return true;
            }

            foreach (Group g in group.ChildGroups)
            {
                if (this.IsDescendentOf(g))
                {
                    return true;
                }
            }

            return false;
        }

        private static void OnDescriptionChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            (obj as Group).IsDirty = true;
        }

        private static void OnNameChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            (obj as Group).IsDirty = true;
        }

        private static void OnTypeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            (obj as Group).IsDirty = true;
        }

        private void ChildGroups_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.IsDirty = true;
        }

        private void ParentGroups_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.IsDirty = true;
        }
    }
}