﻿using System;
using System.Windows;

namespace Mapper
{
    public class TriggerMarker : DraggablePin
    {
        // Using a DependencyProperty as the backing store for IsSelected.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(TriggerMarker), new PropertyMetadata(false));

        // Using a DependencyProperty as the backing store for ParentInsight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ParentInsightProperty =
            DependencyProperty.Register("ParentInsight", typeof(InsightMarker), typeof(TriggerMarker), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for RoadNumber.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RoadNumberProperty =
            DependencyProperty.Register("RoadNumber", typeof(int), typeof(TriggerMarker), new PropertyMetadata(0, new PropertyChangedCallback(OnRoadNumberChanged)));

        // Using a DependencyProperty as the backing store for RoadState.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RoadStateProperty =
            DependencyProperty.Register("RoadState", typeof(string), typeof(TriggerMarker), new PropertyMetadata(string.Empty, new PropertyChangedCallback(OnRoadStateChanged)));

        // Using a DependencyProperty as the backing store for RoadType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RoadTypeProperty =
            DependencyProperty.Register("RoadType", typeof(int), typeof(TriggerMarker), new PropertyMetadata(0, new PropertyChangedCallback(OnRoadTypeChanged)));

        // Using a DependencyProperty as the backing store for TriggerDistance.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TriggerDistanceProperty =
            DependencyProperty.Register("TriggerDistance", typeof(double), typeof(TriggerMarker), new PropertyMetadata(0d, new PropertyChangedCallback(OnTriggerDistanceChanged)));

        // Using a DependencyProperty as the backing store for Id.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty IdProperty =
            DependencyProperty.Register("Id", typeof(Guid), typeof(TriggerMarker), new PropertyMetadata(Guid.Empty));

        // Using a DependencyProperty as the backing store for PostAdGroup.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty PostAdGroupProperty =
            DependencyProperty.Register("PostAdGroup", typeof(Group), typeof(TriggerMarker), new PropertyMetadata(null, new PropertyChangedCallback(OnPostAdGroupChanged)));

        // Using a DependencyProperty as the backing store for PreAdGroup.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty PreAdGroupProperty =
            DependencyProperty.Register("PreAdGroup", typeof(Group), typeof(TriggerMarker), new PropertyMetadata(null, new PropertyChangedCallback(OnPreAdGroupChanged)));

        // Using a DependencyProperty as the backing store for TriggerEast.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty TriggerEastProperty =
            DependencyProperty.Register("TriggerEast", typeof(bool), typeof(TriggerMarker), new PropertyMetadata(false, new PropertyChangedCallback(OnTriggerEastChanged)));

        // Using a DependencyProperty as the backing store for TriggerNorthEast.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty TriggerNorthEastProperty =
            DependencyProperty.Register("TriggerNorthEast", typeof(bool), typeof(TriggerMarker), new PropertyMetadata(false, new PropertyChangedCallback(OnTriggerNorthEastChanged)));

        // Using a DependencyProperty as the backing store for TriggerNorth.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty TriggerNorthProperty =
            DependencyProperty.Register("TriggerNorth", typeof(bool), typeof(TriggerMarker), new PropertyMetadata(false, new PropertyChangedCallback(OnTriggerNorthChanged)));

        // Using a DependencyProperty as the backing store for TriggerNorthWest.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty TriggerNorthWestProperty =
            DependencyProperty.Register("TriggerNorthWest", typeof(bool), typeof(TriggerMarker), new PropertyMetadata(false, new PropertyChangedCallback(OnTriggerNorthWestChanged)));

        // Using a DependencyProperty as the backing store for TriggerSouthEast.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty TriggerSouthEastProperty =
            DependencyProperty.Register("TriggerSouthEast", typeof(bool), typeof(TriggerMarker), new PropertyMetadata(false, new PropertyChangedCallback(OnTriggerSouthEastChanged)));

        // Using a DependencyProperty as the backing store for TriggerSouth.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty TriggerSouthProperty =
            DependencyProperty.Register("TriggerSouth", typeof(bool), typeof(TriggerMarker), new PropertyMetadata(false, new PropertyChangedCallback(OnTriggerSouthChanged)));

        // Using a DependencyProperty as the backing store for TriggerSouthWest.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty TriggerSouthWestProperty =
            DependencyProperty.Register("TriggerSouthWest", typeof(bool), typeof(TriggerMarker), new PropertyMetadata(false, new PropertyChangedCallback(OnTriggerSouthWestChanged)));

        // Using a DependencyProperty as the backing store for TriggerWest.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty TriggerWestProperty =
            DependencyProperty.Register("TriggerWest", typeof(bool), typeof(TriggerMarker), new PropertyMetadata(false, new PropertyChangedCallback(OnTriggerWestChanged)));

        public TriggerMarker(Microsoft.Maps.MapControl.WPF.Map map, InsightMarker parent)
            : base(map)
        {
            this.Id = Guid.NewGuid();
            this.ParentInsight = parent;
        }

        public Guid Id
        {
            get { return (Guid)GetValue(IdProperty); }
            set { SetValue(IdProperty, value); }
        }

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        public InsightMarker ParentInsight
        {
            get { return (InsightMarker)GetValue(ParentInsightProperty); }
            set { SetValue(ParentInsightProperty, value); }
        }

        public Group PostAdGroup
        {
            get { return (Group)GetValue(PostAdGroupProperty); }
            set { SetValue(PostAdGroupProperty, value); }
        }

        public Group PreAdGroup
        {
            get { return (Group)GetValue(PreAdGroupProperty); }
            set { SetValue(PreAdGroupProperty, value); }
        }

        public int RoadNumber
        {
            get { return (int)GetValue(RoadNumberProperty); }
            set { SetValue(RoadNumberProperty, value); }
        }

        public string RoadState
        {
            get { return (string)GetValue(RoadStateProperty); }
            set { SetValue(RoadStateProperty, value); }
        }

        public int RoadType
        {
            get { return (int)GetValue(RoadTypeProperty); }
            set { SetValue(RoadTypeProperty, value); }
        }

        public double TriggerDistance
        {
            get { return (double)GetValue(TriggerDistanceProperty); }
            set { SetValue(TriggerDistanceProperty, value); }
        }

        public bool TriggerEast
        {
            get { return (bool)GetValue(TriggerEastProperty); }
            set { SetValue(TriggerEastProperty, value); }
        }

        public bool TriggerNorth
        {
            get { return (bool)GetValue(TriggerNorthProperty); }
            set { SetValue(TriggerNorthProperty, value); }
        }

        public bool TriggerNorthEast
        {
            get { return (bool)GetValue(TriggerNorthEastProperty); }
            set { SetValue(TriggerNorthEastProperty, value); }
        }

        public bool TriggerNorthWest
        {
            get { return (bool)GetValue(TriggerNorthWestProperty); }
            set { SetValue(TriggerNorthWestProperty, value); }
        }

        public bool TriggerSouth
        {
            get { return (bool)GetValue(TriggerSouthProperty); }
            set { SetValue(TriggerSouthProperty, value); }
        }

        public bool TriggerSouthEast
        {
            get { return (bool)GetValue(TriggerSouthEastProperty); }
            set { SetValue(TriggerSouthEastProperty, value); }
        }

        public bool TriggerSouthWest
        {
            get { return (bool)GetValue(TriggerSouthWestProperty); }
            set { SetValue(TriggerSouthWestProperty, value); }
        }

        public bool TriggerWest
        {
            get { return (bool)GetValue(TriggerWestProperty); }
            set { SetValue(TriggerWestProperty, value); }
        }

        public static void OnPostAdGroupChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnPreAdGroupChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnRoadNumberChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnRoadStateChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnRoadTypeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnTriggerDistanceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnTriggerEastChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnTriggerNorthChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnTriggerNorthEastChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnTriggerNorthWestChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnTriggerSouthChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnTriggerSouthEastChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnTriggerSouthWestChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnTriggerWestChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public override void OnIsDirtyChangedOverride(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is TriggerMarker marker)
            {
                if ((bool)e.NewValue == true)
                {
                    marker.ParentInsight.IsDirty = true;
                }
            }
        }
    }
}