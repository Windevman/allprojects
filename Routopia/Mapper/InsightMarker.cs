﻿namespace Mapper
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;

    public class InsightMarker : DraggablePin
    {
        // Using a DependencyProperty as the backing store for IsSelected.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(InsightMarker), new PropertyMetadata(false));

        // Using a DependencyProperty as the backing store for TriggerList.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TriggerListProperty =
            DependencyProperty.Register("TriggerList", typeof(ObservableCollection<TriggerMarker>), typeof(InsightMarker), new PropertyMetadata(null, new PropertyChangedCallback(OnTriggerListChanged)));

        // Using a DependencyProperty as the backing store for AudioGroup.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty AudioGroupProperty =
            DependencyProperty.Register("AudioGroup", typeof(Group), typeof(InsightMarker), new PropertyMetadata(null, new PropertyChangedCallback(OnAudioGroupChanged)));

        // Using a DependencyProperty as the backing store for Description.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(InsightMarker), new PropertyMetadata(string.Empty, new PropertyChangedCallback(OnDescriptionChanged)));

        // Using a DependencyProperty as the backing store for Id.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty IdProperty =
            DependencyProperty.Register("Id", typeof(Guid), typeof(InsightMarker), new PropertyMetadata(Guid.Empty));

        // Using a DependencyProperty as the backing store for IntroGroup.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty IntroGroupProperty =
            DependencyProperty.Register("IntroGroup", typeof(Group), typeof(InsightMarker), new PropertyMetadata(null, new PropertyChangedCallback(OnIntroGroupChanged)));

        // Using a DependencyProperty as the backing store for OutroGroup.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty OutroGroupProperty =
            DependencyProperty.Register("OutroGroup", typeof(Group), typeof(InsightMarker), new PropertyMetadata(null, new PropertyChangedCallback(OnOutroGroupChanged)));

        // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
        private static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(InsightMarker), new PropertyMetadata(string.Empty, new PropertyChangedCallback(OnTitleChanged)));

        public InsightMarker(Microsoft.Maps.MapControl.WPF.Map map)
            : base(map)
        {
            this.Id = Guid.NewGuid();
            this.TriggerList = new ObservableCollection<TriggerMarker>();
        }

        private void TriggerList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.IsDirty = true;
        }

        public Group AudioGroup
        {
            get { return (Group)GetValue(AudioGroupProperty); }
            set { SetValue(AudioGroupProperty, value); }
        }

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public Guid Id
        {
            get { return (Guid)GetValue(IdProperty); }
            set { SetValue(IdProperty, value); }
        }

        public Group IntroGroup
        {
            get { return (Group)GetValue(IntroGroupProperty); }
            set { SetValue(IntroGroupProperty, value); }
        }

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        public Group OutroGroup
        {
            get { return (Group)GetValue(OutroGroupProperty); }
            set { SetValue(OutroGroupProperty, value); }
        }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public ObservableCollection<TriggerMarker> TriggerList
        {
            get { return (ObservableCollection<TriggerMarker>)GetValue(TriggerListProperty); }
            set { SetValue(TriggerListProperty, value); }
        }

        public static void OnAudioGroupChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is InsightMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnDescriptionChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is InsightMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnIntroGroupChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is InsightMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnOutroGroupChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is InsightMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnTitleChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is InsightMarker marker)
            {
                marker.IsDirty = true;
            }
        }

        public static void OnTriggerListChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is InsightMarker marker)
            {
                if (e.OldValue is ObservableCollection<TriggerMarker> oldList)
                {
                    oldList.CollectionChanged -= marker.TriggerList_CollectionChanged;
                }

                if (e.NewValue is ObservableCollection<TriggerMarker> newList)
                {
                    newList.CollectionChanged += marker.TriggerList_CollectionChanged;
                }
            }
        }

    }
}