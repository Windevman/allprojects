﻿using System.Windows;

namespace Mapper
{
    /// <summary>
    /// Interaction logic for GroupDialog.xaml
    /// </summary>
    public partial class GroupDialog : Window
    {
        // Using a DependencyProperty as the backing store for Description.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(GroupDialog), new PropertyMetadata(string.Empty));

        // Using a DependencyProperty as the backing store for GroupName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GroupNameProperty =
            DependencyProperty.Register("GroupName", typeof(string), typeof(GroupDialog), new PropertyMetadata(string.Empty));

        private Group originalGroup = null;

        public GroupDialog(Group group)
        {
            InitializeComponent();

            this.DataContext = this;

            this.originalGroup = group;

            this.GroupName = group.Name;
            this.Description = group.Description;
        }

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public string GroupName
        {
            get { return (string)GetValue(GroupNameProperty); }
            set { SetValue(GroupNameProperty, value); }
        }

        private void buttonOk_Click(object sender, RoutedEventArgs e)
        {
            originalGroup.Name = this.GroupName;
            originalGroup.Description = this.Description;

            this.DialogResult = true;
            this.Close();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}