﻿using System.Collections.ObjectModel;
using System.Windows;

namespace Mapper
{
    /// <summary>
    /// Interaction logic for GroupSelectorDialog.xaml
    /// </summary>
    public partial class GroupSelectorDialog : Window
    {
        // Using a DependencyProperty as the backing store for Groups.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GroupsProperty =
            DependencyProperty.Register("Groups", typeof(ObservableCollection<Group>), typeof(GroupSelectorDialog), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for SelectedGroup.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedGroupProperty =
            DependencyProperty.Register("SelectedGroup", typeof(Group), typeof(GroupSelectorDialog), new PropertyMetadata(null));

        public GroupSelectorDialog(ObservableCollection<Group> groups, Group selectedGroup)
        {
            InitializeComponent();
            this.DataContext = this;
            this.Groups = groups;
            this.SelectedGroup = selectedGroup;

            gridGroups.Focus();
        }

        public ObservableCollection<Group> Groups
        {
            get { return (ObservableCollection<Group>)GetValue(GroupsProperty); }
            set { SetValue(GroupsProperty, value); }
        }

        public Group SelectedGroup
        {
            get { return (Group)GetValue(SelectedGroupProperty); }
            set { SetValue(SelectedGroupProperty, value); }
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void buttonOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }
    }
}