﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="RoutopiaCloudService" generation="1" functional="0" release="0" Id="bde9e37c-2e2d-4179-943a-48bcfd2ccffb" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="RoutopiaCloudServiceGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="RoutopiaWebRole:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/RoutopiaCloudService/RoutopiaCloudServiceGroup/LB:RoutopiaWebRole:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="RoutopiaWebRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/RoutopiaCloudService/RoutopiaCloudServiceGroup/MapRoutopiaWebRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="RoutopiaWebRoleInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/RoutopiaCloudService/RoutopiaCloudServiceGroup/MapRoutopiaWebRoleInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:RoutopiaWebRole:Endpoint1">
          <toPorts>
            <inPortMoniker name="/RoutopiaCloudService/RoutopiaCloudServiceGroup/RoutopiaWebRole/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapRoutopiaWebRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/RoutopiaCloudService/RoutopiaCloudServiceGroup/RoutopiaWebRole/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapRoutopiaWebRoleInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/RoutopiaCloudService/RoutopiaCloudServiceGroup/RoutopiaWebRoleInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="RoutopiaWebRole" generation="1" functional="0" release="0" software="C:\SVNProjects\trunk\Routopia\RoutopiaCloudService\csx\Release\roles\RoutopiaWebRole" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;RoutopiaWebRole&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;RoutopiaWebRole&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/RoutopiaCloudService/RoutopiaCloudServiceGroup/RoutopiaWebRoleInstances" />
            <sCSPolicyUpdateDomainMoniker name="/RoutopiaCloudService/RoutopiaCloudServiceGroup/RoutopiaWebRoleUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/RoutopiaCloudService/RoutopiaCloudServiceGroup/RoutopiaWebRoleFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="RoutopiaWebRoleUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="RoutopiaWebRoleFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="RoutopiaWebRoleInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="90983b7a-e846-44db-b9a9-a3aae3f86807" ref="Microsoft.RedDog.Contract\ServiceContract\RoutopiaCloudServiceContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="e93072ba-bec7-4152-9d9d-2bf52b148f97" ref="Microsoft.RedDog.Contract\Interface\RoutopiaWebRole:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/RoutopiaCloudService/RoutopiaCloudServiceGroup/RoutopiaWebRole:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>