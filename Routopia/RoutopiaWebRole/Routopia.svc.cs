﻿using Routopia.Shared;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.Serialization.Json;

namespace RoutopiaWebRole
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class RoutopiaService : IRoutopiaService
    {
        private readonly string connectionString = @"Server=tcp:routopiadbserver.database.windows.net,1433;Initial Catalog=Routopia;Persist Security Info=False;User ID=mattolsen;Password=Id0ntknow;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        /// <summary>
        /// Adds a delete history item
        /// </summary>
        /// <param name="connection">The connection to the database</param>
        /// <param name="primary">The primary key</param>
        /// <param name="secondary">The secondary key</param>
        /// <param name="type">The record type</param>
        public void AddDeleteHistory(SqlConnection connection, Guid primary, Guid? secondary, DeleteHistoryTypes type)
        {
            string commandText = "INSERT INTO DeletionHistory (PrimaryId, SecondaryId, DeletionDate, RecordType) " +
                "VALUES (@primaryid, @secondaryid, @deletiondate, @recordtype)";

            SqlCommand command = new SqlCommand(commandText, connection);
            command.Parameters.AddWithValue("@primaryid", primary);
            command.Parameters.AddWithValue("@secondaryid", secondary ?? Guid.Empty);
            command.Parameters.AddWithValue("@deletiondate", DateTime.UtcNow);
            command.Parameters.AddWithValue("@recordtype", type);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Attaches audio to location
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AttachAudioResponse AttachAudio(AttachAudioRequest request)
        {
            AttachAudioResponse response = new AttachAudioResponse();

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string queryString = "SELECT COUNT(*) from [dbo].[AudioGroupLinks] WHERE [AudioGroupId] = @audiogroupid AND [AudioClipId] = @audioclipid";

                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@audiogroupid", request.AudioGroupId);
                    command.Parameters.AddWithValue("@audioclipid", request.AudioId);
                    int count = (int)command.ExecuteScalar();

                    connection.Close();

                    if (count == 0)
                    {
                        connection.Open();

                        string commandText = "INSERT INTO AudioGroupLinks (AudioGroupId, AudioClipId) " +
                            "VALUES (@audiogroupid, @audioclipid)";

                        command = new SqlCommand(commandText, connection);
                        command.Parameters.AddWithValue("@audiogroupid", request.AudioGroupId);
                        command.Parameters.AddWithValue("@audioclipid", request.AudioId);
                        command.ExecuteNonQuery();

                        connection.Close();
                    }
                }

                response.Result = true;
            }
            catch (System.Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.Result = false;
            }

            return response;
        }

        //public AttachGroupsResponse AttachGroups(AttachGroupsRequest request)
        //{
        //    AttachGroupsResponse response = new AttachGroupsResponse();

        //    try
        //    {
        //        using (SqlConnection connection = new SqlConnection(connectionString))
        //        {
        //            connection.Open();

        //            string queryString = "SELECT COUNT(*) from [dbo].[GroupLinks] WHERE [ParentGroupId] = @parentgroupid AND [ChildGroupId] = @childgroupid";

        //            SqlCommand command = new SqlCommand(queryString, connection);
        //            command.Parameters.AddWithValue("@parentgroupid", request.AudioGroupParent);
        //            command.Parameters.AddWithValue("@childgroupid", request.AudioGroupChild);
        //            int count = (int)command.ExecuteScalar();

        //            connection.Close();

        //            if (count == 0)
        //            {
        //                connection.Open();

        //                string commandText = "INSERT INTO GroupLinks (ParentGroupId, ChildGroupId) " +
        //                    "VALUES (@parentgroupid, @childgroupid)";

        //                command = new SqlCommand(commandText, connection);
        //                command.Parameters.AddWithValue("@parentgroupid", request.AudioGroupParent);
        //                command.Parameters.AddWithValue("@childgroupid", request.AudioGroupChild);
        //                command.ExecuteNonQuery();

        //                connection.Close();
        //            }
        //        }

        //        response.Result = true;
        //    }
        //    catch (System.Exception ex)
        //    {
        //        response.ErrorMessage = ex.Message;
        //        response.Result = false;
        //    }

        //    return response;
        //}

        public DeleteAudioClipsResponse DeleteAudioClips(DeleteAudioClipsRequest request)
        {
            DeleteAudioClipsResponse response = new DeleteAudioClipsResponse();

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    List<string> list = new List<string>();

                    foreach (Guid guid in request.AudioClipIdList)
                    {
                        list.Add($"'{guid.ToString("D")}'");
                    }

                    string commandText = $"DELETE AudioGroupLinks WHERE AudioClipId IN ({string.Join(",", list)})";
                    SqlCommand command = new SqlCommand(commandText, connection);
                    int count = (int)command.ExecuteNonQuery();

                    commandText = $"DELETE AudioClips WHERE Id IN ({string.Join(",", list)})";
                    command = new SqlCommand(commandText, connection);
                    count = (int)command.ExecuteNonQuery();

                    if (count > 0)
                    {
                        foreach (Guid guid in request.AudioClipIdList)
                        {
                            this.AddDeleteHistory(connection, guid, null, DeleteHistoryTypes.AudioClips);
                        }
                    }

                    commandText = $"DELETE AudioGroupLinks WHERE AudioClipId IN ({string.Join(",", list)})";
                    command = new SqlCommand(commandText, connection);
                    count = (int)command.ExecuteNonQuery();

                    connection.Close();

                    response.Result = true;
                }

                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.ErrorMessage = ex.Message;
                response.Result = false;
            }

            return response;
        }

        public DeleteAudioGroupLinkResponse DeleteAudioGroupLink(DeleteAudioGroupLinkRequest request)
        {
            throw new NotImplementedException();
        }

        public DeleteAudioGroupResponse DeleteAudioGroups(DeleteAudioGroupRequest request)
        {
            DeleteAudioGroupResponse response = new DeleteAudioGroupResponse();

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    List<string> list = new List<string>();

                    foreach (Guid guid in request.AudioGroupIdList)
                    {
                        list.Add($"'{guid.ToString("D")}'");
                    }

                    string inList = string.Join(",", list);

                    // Delete from any insights
                    string commandText = $"UPDATE Insights SET IntroGroupId = NULL WHERE IntroGroupId IN ({inList})";
                    SqlCommand command = new SqlCommand(commandText, connection);
                    int count = (int)command.ExecuteNonQuery();

                    commandText = $"UPDATE Insights SET AudioId = NULL WHERE AudioId IN ({inList})";
                    command = new SqlCommand(commandText, connection);
                    count = (int)command.ExecuteNonQuery();

                    commandText = $"UPDATE Insights SET OutroGroupId = NULL WHERE OUtroGroupId IN ({inList})";
                    command = new SqlCommand(commandText, connection);
                    count = (int)command.ExecuteNonQuery();

                    // Delete from any triggers
                    commandText = $"UPDATE Triggers SET PreAdGroupId = NULL WHERE PreAdGroupId IN ({inList})";
                    command = new SqlCommand(commandText, connection);
                    count = (int)command.ExecuteNonQuery();

                    commandText = $"UPDATE Triggers SET PostAdGroupId = NULL WHERE PostAdGroupId IN ({inList})";
                    command = new SqlCommand(commandText, connection);
                    count = (int)command.ExecuteNonQuery();

                    // Delete the location
                    commandText = $"DELETE AudioGroups WHERE Id IN ({inList})";
                    command = new SqlCommand(commandText, connection);
                    count = (int)command.ExecuteNonQuery();

                    if (count > 0)
                    {
                        foreach (Guid guid in request.AudioGroupIdList)
                        {
                            this.AddDeleteHistory(connection, guid, null, DeleteHistoryTypes.AudioGroups);
                        }
                    }

                    connection.Close();

                    response.Result = true;
                }

                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.ErrorMessage = ex.Message;
                response.Result = false;
            }

            return response;
        }

        /// <summary>
        /// Deletes the cache record with the specified id
        /// </summary>
        /// <param name="request">The request record containing the id of the cache to delete</param>
        /// <returns>A response object</returns>
        public DeleteCacheResponse DeleteCache(DeleteCacheRequest request)
        {
            DeleteCacheResponse response = new DeleteCacheResponse();

            try
            {
                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "DELETE [dbo].[DataCache] " +
                    "WHERE [Id]=@id";

                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@id", request.CacheId);

                    command.Connection.Open();

                    command.ExecuteNonQuery();

                    response.Result = true;
                }
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.Result = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public DeleteGroupLinksResponse DeleteGroupLinks(DeleteGroupLinksRequest request)
        {
            throw new NotImplementedException();
        }

        public DeleteInsightResponse DeleteInsight(DeleteInsightRequest request)
        {
            DeleteInsightResponse response = new DeleteInsightResponse();

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    // Delete out the zone points
                    string commandText = "DELETE Triggers WHERE InsightId=@insightid";
                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.Parameters.AddWithValue("@insightid", request.InsightId);
                    command.ExecuteNonQuery();

                    // Delete the location
                    commandText = "DELETE Insights WHERE Id=@insightid";
                    command = new SqlCommand(commandText, connection);
                    command.Parameters.AddWithValue("@insightid", request.InsightId);
                    int count = (int)command.ExecuteNonQuery();

                    if (count > 0)
                    {
                        this.AddDeleteHistory(connection, request.InsightId, null, DeleteHistoryTypes.Insight);
                    }

                    connection.Close();

                    response.Result = true;
                }

                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.ErrorMessage = ex.Message;
                response.Result = false;
            }

            return response;
        }

        /// <summary>
        /// Attaches audio to location
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DetachAudioResponse DetachAudio(DetachAudioRequest request)
        {
            DetachAudioResponse response = new DetachAudioResponse();

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string queryString = "DELETE [dbo].[AudioGroupLinks] WHERE [AudioGroupId] = @audiogroupid AND [AudioClipId] = @audioclipid";

                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@audiogroupid", request.AudioGroupId);
                    command.Parameters.AddWithValue("@audioclipid", request.AudioId);
                    int count = (int)command.ExecuteNonQuery();

                    if (count > 0)
                    {
                        this.AddDeleteHistory(connection, request.AudioGroupId, request.AudioId, DeleteHistoryTypes.AudioGroupLinks);
                    }

                    connection.Close();
                }

                response.Result = true;
            }
            catch (System.Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.Result = false;
            }

            return response;
        }

        public DetachGroupResponse DetachGroup(DetachGroupRequest request)
        {
            DetachGroupResponse response = new DetachGroupResponse();

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string queryString = "DELETE [dbo].[GroupLinks] WHERE [ParentGroupId] = @parentgroupid AND [ChildGroupId] = @childgroupid";

                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@parentgroupid", request.AudioGroupParent);
                    command.Parameters.AddWithValue("@childgroupid", request.AudioGroupChild);
                    int count = (int)command.ExecuteNonQuery();

                    if (count > 0)
                    {
                        this.AddDeleteHistory(connection, request.AudioGroupParent, request.AudioGroupChild, DeleteHistoryTypes.GroupLinks);
                    }

                    connection.Close();
                }

                response.Result = true;
            }
            catch (System.Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.Result = false;
            }

            return response;
        }

        public string[] GetURLsById(Guid? parentId)
        {
            List<string> urls = new List<string>();

            if (parentId == null)
            {
                return urls.ToArray();
            }

            try
            {
                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "SELECT[URL] FROM AudioClips WHERE Id IN(" +
                    "SELECT AudioClipId FROM AudioGroupLinks " +
                    "WHERE AudioGroupId = @id)";

                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@id", parentId);
                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        urls.Add(reader.GetString(0));
                    }

                    command.Connection.Close();
                }

                List<Guid> guids = new List<Guid>();

                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "SELECT ChildGroupId FROM GroupLinks " +
                    "WHERE ParentGroupId = @id";

                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@id", parentId);
                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        guids.Add(reader.GetGuid(0));
                    }

                    command.Connection.Close();

                    foreach (Guid guid in guids)
                    {
                        string[] moreUrls = this.GetURLsById(guid);

                        foreach (string url in moreUrls)
                        {
                            if (urls.Contains(url) == false)
                            {
                                urls.Add(url);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
            }

            return urls.ToArray();
        }

        /// <summary>
        /// Logs the specified message to the database
        /// </summary>
        /// <param name="message">The message to log</param>
        public void Log(string message)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string commandText = "INSERT INTO Logs (Data) VALUES (@data)";

                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.Parameters.AddWithValue("@data", message);
                    command.ExecuteNonQuery();

                    connection.Close();
                }
            }
            catch (System.Exception ex)
            {
            }
        }

        public ReadAudioClipsResponse ReadAudioClips(ReadAudioClipsRequest request)
        {
            ReadAudioClipsResponse response = new ReadAudioClipsResponse();

            try
            {
                List<AudioClip> clips = new List<AudioClip>();

                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "SELECT[Id]" +
                    ",[Name]" +
                    ",[Description]" +
                    ",[Type]" +
                    ",[URL]" +
                    " FROM[dbo].[AudioClips]";

                    SqlCommand command = new SqlCommand(queryString, connection);

                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        AudioClip clip = new AudioClip()
                        {
                            Id = reader.GetGuid(0),
                            Name = reader.GetString(1),
                            Description = reader.GetString(2),
                            Type = reader.GetInt32(3),
                            URL = reader.GetString(4)
                        };

                        clips.Add(clip);
                    }

                    command.Connection.Close();
                }

                response.CacheId = this.SaveCache<AudioClip>(clips, DataCacheTypes.Temporary);
                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.Result = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public ReadAudioGroupLinksResponse ReadAudioGroupLinks(ReadAudioGroupLinksRequest request)
        {
            ReadAudioGroupLinksResponse response = new ReadAudioGroupLinksResponse();

            try
            {
                List<AudioGroupLink> links = new List<AudioGroupLink>();

                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "SELECT[AudioGroupId]" +
                    ",[AudioClipId] " +
                    "FROM[dbo].[AudioGroupLinks]";

                    SqlCommand command = new SqlCommand(queryString, connection);

                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        AudioGroupLink link = new AudioGroupLink()
                        {
                            AudioGroupId = reader.GetGuid(0),
                            AudioClipId = reader.GetGuid(1)
                        };

                        links.Add(link);
                    }

                    command.Connection.Close();
                }

                response.CacheId = this.SaveCache<AudioGroupLink>(links, DataCacheTypes.Temporary);
                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.Result = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public ReadAudioGroupsResponse ReadAudioGroups(ReadAudioGroupsRequest request)
        {
            ReadAudioGroupsResponse response = new ReadAudioGroupsResponse();

            try
            {
                List<AudioGroup> groups = new List<AudioGroup>();

                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "SELECT[Id]" +
                    ",[Name]" +
                    ",[Description]" +
                    ",[Type]" +
                    " FROM[dbo].[AudioGroups]";

                    SqlCommand command = new SqlCommand(queryString, connection);

                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        AudioGroup group = new AudioGroup()
                        {
                            Id = reader.GetGuid(0),
                            Name = reader.GetString(1),
                            Description = reader.GetString(2),
                            Type = reader.GetInt32(3)
                        };

                        groups.Add(group);
                    }

                    command.Connection.Close();
                }

                response.CacheId = this.SaveCache<AudioGroup>(groups, DataCacheTypes.Temporary);
                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.Result = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public ReadAudioTypesResponse ReadAudioTypes(ReadAudioTypesRequest request)
        {
            ReadAudioTypesResponse response = new ReadAudioTypesResponse();

            try
            {
                List<AudioType> types = new List<AudioType>();

                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "SELECT[Id]" +
                    ",[Name] " +
                    "FROM[dbo].[AudioTypes]";

                    SqlCommand command = new SqlCommand(queryString, connection);

                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        AudioType type = new AudioType()
                        {
                            Id = reader.GetInt32(0),
                            Name = reader.GetString(1)
                        };

                        types.Add(type);
                    }

                    command.Connection.Close();
                }

                response.CacheId = this.SaveCache<AudioType>(types, DataCacheTypes.Temporary);
                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.Result = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Reads the specified cache segment
        /// </summary>
        /// <param name="request">The request containing the cache request and sequence id</param>
        /// <returns>A response object containing the requested data</returns>
        public ReadCacheResponse ReadCache(ReadCacheRequest request)
        {
            ReadCacheResponse response = new ReadCacheResponse();

            try
            {
                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "SELECT[Id]" +
                    ",[Sequence]" +
                    ",[Data]" +
                    ",[Length]" +
                    ",[LastSegment]" +
                    "FROM[dbo].[DataCache] " +
                    "WHERE [Id]=@id AND [Sequence]=@sequence ORDER BY [Sequence]";

                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@id", request.CacheId);
                    command.Parameters.AddWithValue("@sequence", request.Sequence);

                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    byte[] buffer = null;
                    bool lastSegment = false;

                    while (reader.Read())
                    {
                        Guid id = reader.GetGuid(0);
                        int sequence = reader.GetInt32(1);
                        int length = reader.GetInt32(3);
                        lastSegment = reader.GetBoolean(4);

                        buffer = new byte[length];

                        reader.GetBytes(2, 0, buffer, 0, buffer.Length);

                        break;
                    }

                    command.Connection.Close();

                    response.Data = buffer;
                    response.Result = true;
                    response.Complete = lastSegment;
                }
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.Result = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Reads all the deletion history
        /// </summary>
        /// <param name="request">The request object</param>
        /// <returns>A response object</returns>
        public ReadDeletionHistoryResponse ReadDeletionHistory(ReadDeletionHistoryRequest request)
        {
            ReadDeletionHistoryResponse response = new ReadDeletionHistoryResponse();

            try
            {
                List<DeletionHistory> histories = new List<DeletionHistory>();

                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "SELECT[Id]" +
                    ",[PrimaryId] " +
                    ",[SecondaryId] " +
                    ",[DeletionDate] " +
                    ",[RecordType] " +
                    "FROM[dbo].[DeletionHistory]";

                    if (request.LastUpdate != null)
                    {
                        queryString += $" WHERE DeletionDate > @lastupdate";
                    }

                    SqlCommand command = new SqlCommand(queryString, connection);

                    if (request.LastUpdate != null)
                    {
                        command.Parameters.AddWithValue("@lastupdate", request.LastUpdate);
                    }

                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        DeletionHistory hist = new DeletionHistory()
                        {
                            Id = reader.GetInt32(0),
                            PrimaryId = reader.GetGuid(1),
                            SecondaryId = reader.GetGuid(2),
                            DeletionDate = reader.GetDateTime(3),
                            Type = (DeleteHistoryTypes)reader.GetInt32(4)
                        };

                        histories.Add(hist);
                    }

                    command.Connection.Close();
                }

                response.CacheId = this.SaveCache<DeletionHistory>(histories, DataCacheTypes.Temporary);
                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.Result = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public ReadGroupLinksResponse ReadGroupLinks(ReadGroupLinksRequest request)
        {
            ReadGroupLinksResponse response = new ReadGroupLinksResponse();

            try
            {
                List<GroupLink> links = new List<GroupLink>();

                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "SELECT[ParentGroupId]" +
                    ",[ChildGroupId] " +
                    "FROM[dbo].[GroupLinks]";

                    SqlCommand command = new SqlCommand(queryString, connection);

                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        GroupLink link = new GroupLink()
                        {
                            ParentGroupId = reader.GetGuid(0),
                            ChildGroupId = reader.GetGuid(1)
                        };

                        links.Add(link);
                    }

                    command.Connection.Close();
                }

                response.CacheId = this.SaveCache<GroupLink>(links, DataCacheTypes.Temporary);
                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.Result = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public ReadInsightsResponse ReadInsights(ReadInsightsRequest request)
        {
            ReadInsightsResponse response = new ReadInsightsResponse();

            try
            {
                List<Insight> insights = new List<Insight>();

                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "SELECT[Id]" +
                    ",[Name]" +
                    ",[Description]" +
                    ",[Longitude]" +
                    ",[Latitude]" +
                    ",[IntroGroupId]" +
                    ",[AudioId]" +
                    ",[OutroGroupId]" +
                    ",[RegionId]" +
                    " FROM[dbo].[Insights]";

                    SqlCommand command = new SqlCommand(queryString, connection);

                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        Insight insight = new Insight()
                        {
                            Id = reader.GetGuid(0),
                            Name = reader.GetString(1),
                            Description = reader.GetString(2),
                            Longitude = reader.GetDouble(3),
                            Latitude = reader.GetDouble(4),
                            IntroGroupId = reader.IsDBNull(5) == true ? (Guid?)null : reader.GetGuid(5),
                            AudioId = reader.IsDBNull(6) == true ? (Guid?)null : reader.GetGuid(6),
                            OutroGroupId = reader.IsDBNull(7) == true ? (Guid?)null : reader.GetGuid(7),
                            RegionId = reader.GetString(8)
                        };

                        insights.Add(insight);
                    }

                    command.Connection.Close();

                    foreach (Insight insight in insights)
                    {
                        queryString =
                        "SELECT [Id]" +
                        ",[Latitude]" +
                        ",[Longitude]" +
                        ",[TriggerNorth]" +
                        ",[TriggerEast]" +
                        ",[TriggerSouth]" +
                        ",[TriggerWest]" +
                        ",[TriggerNorthEast]" +
                        ",[TriggerSouthEast]" +
                        ",[TriggerSouthWest]" +
                        ",[TriggerNorthWest]" +
                        ",[PreAdGroupId]" +
                        ",[PostAdGroupId]" +
                        ",[RoadType]" +
                        ",[RoadNumber]" +
                        ",[RoadState]" +
                        ",[TriggerDistance] " +
                        "FROM [dbo].[Triggers]" +
                        "WHERE InsightId=@insightId";

                        command = new SqlCommand(queryString, connection);
                        command.Parameters.AddWithValue("@insightId", insight.Id);

                        command.Connection.Open();

                        reader = command.ExecuteReader();

                        List<Trigger> triggers = new List<Trigger>();

                        while (reader.Read())
                        {
                            Trigger trigger = new Trigger()
                            {
                                Id = reader.GetGuid(0),
                                Latitude = reader.GetDouble(1),
                                Longitude = reader.GetDouble(2),
                                TriggerNorth = reader.GetBoolean(3),
                                TriggerEast = reader.GetBoolean(4),
                                TriggerSouth = reader.GetBoolean(5),
                                TriggerWest = reader.GetBoolean(6),
                                TriggerNorthEast = reader.GetBoolean(7),
                                TriggerSouthEast = reader.GetBoolean(8),
                                TriggerSouthWest = reader.GetBoolean(9),
                                TriggerNorthWest = reader.GetBoolean(10),
                                PreAdGroupId = reader.IsDBNull(11) == true ? (Guid?)null : reader.GetGuid(11),
                                PostAdGroupId = reader.IsDBNull(12) == true ? (Guid?)null : reader.GetGuid(12),
                                RoadType = (RoadTypes)reader.GetInt32(13),
                                RoadNumber = reader.GetInt32(14),
                                RoadState = reader.GetString(15),
                                TriggerDistance = reader.GetDouble(16)
                            };

                            triggers.Add(trigger);
                        }

                        insight.Triggers = triggers.ToArray();

                        command.Connection.Close();
                    }
                }

                response.CacheId = this.SaveCache<Insight>(insights, DataCacheTypes.Temporary);
                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.Result = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public ReadInsightsByLocationResponse ReadInsightsByLocation(ReadInsightsByLocationRequest request)
        {
            ReadInsightsByLocationResponse response = new ReadInsightsByLocationResponse();

            try
            {
                List<Guid> insightsToPull = new List<Guid>();
                List<Trigger> triggers = new List<Trigger>();

                // Get all the triggers
                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "SELECT [Id]" +
                    ",[Latitude]" +
                    ",[Longitude]" +
                    ",[InsightId] " +
                    "FROM [dbo].[Triggers]";

                    SqlCommand command = new SqlCommand(queryString, connection);

                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        Trigger t = new Trigger()
                        {
                            Id = reader.GetGuid(0),
                            Latitude = reader.GetDouble(1),
                            Longitude = reader.GetDouble(2),
                            InsightId = reader.GetGuid(3)
                        };

                        triggers.Add(t);
                    }

                    command.Connection.Close();
                }

                // Find any triggers that are within the proper distance
                double lat1 = request.Latitude;
                double lon1 = request.Longitude;

                foreach (Trigger t in triggers)
                {
                    double lat2 = t.Latitude;
                    double lon2 = t.Longitude;

                    var R = 6371e3; // Earth's radius in meters

                    double rc = Math.PI / 180;

                    var latRad1 = lat1 * rc;
                    var latRad2 = lat2 * rc;
                    var deltaLat = (lat2 - lat1) * rc;
                    var deltaLon = (lon2 - lon1) * rc;

                    var a = Math.Sin(deltaLat / 2) * Math.Sin(deltaLat / 2) +
                            Math.Cos(latRad1) * Math.Cos(latRad2) *
                            Math.Sin(deltaLon / 2) * Math.Sin(deltaLon / 2);

                    var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

                    var d = R * c;
                    var m = (d / 1000) * 0.621371;

                    if (m <= request.DistanceInMiles)
                    {
                        if (insightsToPull.Contains(t.InsightId.Value) == false)
                        {
                            insightsToPull.Add(t.InsightId.Value);
                        }
                    }
                }

                // Read the insights from the database (50 at a time)
                List<Insight> insights = new List<Insight>();

                while (insightsToPull.Count > 0)
                {
                    int pullCount = 50;

                    if (pullCount > insightsToPull.Count)
                    {
                        pullCount = insightsToPull.Count;
                    }

                    var pulledItems = insightsToPull.GetRange(0, pullCount);

                    List<string> list = new List<string>();

                    foreach (Guid guid in pulledItems)
                    {
                        list.Add($"'{guid.ToString("D")}'");
                    }

                    using (SqlConnection connection = new SqlConnection(
                               connectionString))
                    {
                        string queryString =
                        "SELECT[Id]" +
                        ",[Name]" +
                        ",[Description]" +
                        ",[Longitude]" +
                        ",[Latitude]" +
                        ",[IntroGroupId]" +
                        ",[AudioId]" +
                        ",[OutroGroupId] " +
                        ",[RegionId] " +
                        "FROM[dbo].[Insights] " +
                       $"WHERE Id IN ({string.Join(",", list)})";

                        SqlCommand command = new SqlCommand(queryString, connection);

                        command.Connection.Open();

                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Insight insight = new Insight()
                            {
                                Id = reader.GetGuid(0),
                                Name = reader.GetString(1),
                                Description = reader.GetString(2),
                                Longitude = reader.GetDouble(3),
                                Latitude = reader.GetDouble(4),
                                IntroGroupId = reader.IsDBNull(5) == true ? (Guid?)null : reader.GetGuid(5),
                                AudioId = reader.IsDBNull(6) == true ? (Guid?)null : reader.GetGuid(6),
                                OutroGroupId = reader.IsDBNull(7) == true ? (Guid?)null : reader.GetGuid(7),
                                RegionId = reader.GetString(8)
                            };

                            insight.AudioURLs = this.GetURLsById(insight.AudioId);
                            insight.IntroURLs = this.GetURLsById(insight.IntroGroupId);
                            insight.OutroURLs = this.GetURLsById(insight.OutroGroupId);

                            insights.Add(insight);
                        }

                        command.Connection.Close();

                        foreach (Insight insight in insights)
                        {
                            queryString =
                            "SELECT [Id]" +
                            ",[Latitude]" +
                            ",[Longitude]" +
                            ",[TriggerNorth]" +
                            ",[TriggerEast]" +
                            ",[TriggerSouth]" +
                            ",[TriggerWest]" +
                            ",[TriggerNorthEast]" +
                            ",[TriggerSouthEast]" +
                            ",[TriggerSouthWest]" +
                            ",[TriggerNorthWest]" +
                            ",[PreAdGroupId]" +
                            ",[PostAdGroupId]" +
                            ",[RoadType]" +
                            ",[RoadNumber]" +
                            ",[RoadState]" +
                            ",[TriggerDistance] " +
                            "FROM [dbo].[Triggers]" +
                            "WHERE InsightId=@insightId";

                            command = new SqlCommand(queryString, connection);
                            command.Parameters.AddWithValue("@insightId", insight.Id);

                            command.Connection.Open();

                            reader = command.ExecuteReader();

                            List<Trigger> triggers2 = new List<Trigger>();

                            while (reader.Read())
                            {
                                Trigger trigger = new Trigger()
                                {
                                    Id = reader.GetGuid(0),
                                    Latitude = reader.GetDouble(1),
                                    Longitude = reader.GetDouble(2),
                                    TriggerNorth = reader.GetBoolean(3),
                                    TriggerEast = reader.GetBoolean(4),
                                    TriggerSouth = reader.GetBoolean(5),
                                    TriggerWest = reader.GetBoolean(6),
                                    TriggerNorthEast = reader.GetBoolean(7),
                                    TriggerSouthEast = reader.GetBoolean(8),
                                    TriggerSouthWest = reader.GetBoolean(9),
                                    TriggerNorthWest = reader.GetBoolean(10),
                                    PreAdGroupId = reader.IsDBNull(11) == true ? (Guid?)null : reader.GetGuid(11),
                                    PostAdGroupId = reader.IsDBNull(12) == true ? (Guid?)null : reader.GetGuid(12),
                                    RoadType = (RoadTypes)reader.GetInt32(13),
                                    RoadNumber = reader.GetInt32(14),
                                    RoadState = reader.GetString(15),
                                    TriggerDistance = reader.GetDouble(16)
                                };

                                trigger.PostAdURLs = this.GetURLsById(trigger.PostAdGroupId);
                                trigger.PreAdURLs = this.GetURLsById(trigger.PreAdGroupId);

                                triggers2.Add(trigger);
                            }

                            insight.Triggers = triggers2.ToArray();

                            command.Connection.Close();
                        }
                    }

                    insightsToPull.RemoveRange(0, pullCount);
                }

                ////foreach (Insight insight in insights)
                ////{
                ////    foreach (Trigger trigger in insight.Triggers)
                ////    {
                ////        PointOfInterest poi = new PointOfInterest()
                ////        {
                ////            Description = insight.Description,
                ////            InsightId = insight.Id,
                ////            Latitude = trigger.Latitude,
                ////            Longitude = trigger.Longitude,
                ////            Name = insight.Name,
                ////            RoadNumber = trigger.RoadNumber,
                ////            RoadType = (int)trigger.RoadType,
                ////            RoadState = trigger.RoadState,
                ////            TriggerId = trigger.Id,
                ////            TriggerEast = trigger.TriggerEast,
                ////            TriggerNorth = trigger.TriggerNorth,
                ////            TriggerNorthEast = trigger.TriggerNorthEast,
                ////            TriggerNorthWest = trigger.TriggerNorthWest,
                ////            TriggerSouth = trigger.TriggerSouth,
                ////            TriggerSouthEast = trigger.TriggerSouthEast,
                ////            TriggerSouthWest = trigger.TriggerSouthWest,
                ////            TriggerWest = trigger.TriggerWest,
                ////            TriggerDistance = trigger.TriggerDistance,
                ////            AudioURLs = this.GetURLsById(insight.AudioId),
                ////            IntroURLs = this.GetURLsById(insight.IntroGroupId),
                ////            OutroURLs = this.GetURLsById(insight.OutroGroupId),
                ////            PostAdURLs = this.GetURLsById(trigger.PostAdGroupId),
                ////            PreAdURLs = this.GetURLsById(trigger.PreAdGroupId)
                ////        };

                ////        pointOfInterests.Add(poi);
                ////    }
                ////}

                response.CacheId = this.SaveCache<Insight>(insights, DataCacheTypes.Temporary);
                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.Result = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public ReadInsightsByRegionResponse ReadInsightsByRegion(ReadInsightsByRegionRequest request)
        {
            ReadInsightsByRegionResponse response = new ReadInsightsByRegionResponse();

            try
            {
                List<Guid> insightsToPull = new List<Guid>();
                List<Trigger> triggers = new List<Trigger>();

                // Read the insights from the database
                List<Insight> insights = new List<Insight>();

                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "SELECT[Id]" +
                    ",[Name]" +
                    ",[Description]" +
                    ",[Longitude]" +
                    ",[Latitude]" +
                    ",[IntroGroupId]" +
                    ",[AudioId]" +
                    ",[OutroGroupId] " +
                    ",[RegionId]" +
                    "FROM[dbo].[Insights] " +
                   $"WHERE RegionId IN ('{string.Join("','", request.Regions)}')";

                    SqlCommand command = new SqlCommand(queryString, connection);

                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        Insight insight = new Insight()
                        {
                            Id = reader.GetGuid(0),
                            Name = reader.GetString(1),
                            Description = reader.GetString(2),
                            Longitude = reader.GetDouble(3),
                            Latitude = reader.GetDouble(4),
                            IntroGroupId = reader.IsDBNull(5) == true ? (Guid?)null : reader.GetGuid(5),
                            AudioId = reader.IsDBNull(6) == true ? (Guid?)null : reader.GetGuid(6),
                            OutroGroupId = reader.IsDBNull(7) == true ? (Guid?)null : reader.GetGuid(7),
                            RegionId = reader.GetString(8)
                        };

                        insight.AudioURLs = this.GetURLsById(insight.AudioId);
                        insight.IntroURLs = this.GetURLsById(insight.IntroGroupId);
                        insight.OutroURLs = this.GetURLsById(insight.OutroGroupId);

                        insights.Add(insight);
                    }

                    command.Connection.Close();

                    foreach (Insight insight in insights)
                    {
                        queryString =
                        "SELECT [Id]" +
                        ",[Latitude]" +
                        ",[Longitude]" +
                        ",[TriggerNorth]" +
                        ",[TriggerEast]" +
                        ",[TriggerSouth]" +
                        ",[TriggerWest]" +
                        ",[TriggerNorthEast]" +
                        ",[TriggerSouthEast]" +
                        ",[TriggerSouthWest]" +
                        ",[TriggerNorthWest]" +
                        ",[PreAdGroupId]" +
                        ",[PostAdGroupId]" +
                        ",[RoadType]" +
                        ",[RoadNumber]" +
                        ",[RoadState]" +
                        ",[TriggerDistance] " +
                        "FROM [dbo].[Triggers]" +
                        "WHERE InsightId=@insightId";

                        command = new SqlCommand(queryString, connection);
                        command.Parameters.AddWithValue("@insightId", insight.Id);

                        command.Connection.Open();

                        reader = command.ExecuteReader();

                        List<Trigger> triggers2 = new List<Trigger>();

                        while (reader.Read())
                        {
                            Trigger trigger = new Trigger()
                            {
                                Id = reader.GetGuid(0),
                                Latitude = reader.GetDouble(1),
                                Longitude = reader.GetDouble(2),
                                TriggerNorth = reader.GetBoolean(3),
                                TriggerEast = reader.GetBoolean(4),
                                TriggerSouth = reader.GetBoolean(5),
                                TriggerWest = reader.GetBoolean(6),
                                TriggerNorthEast = reader.GetBoolean(7),
                                TriggerSouthEast = reader.GetBoolean(8),
                                TriggerSouthWest = reader.GetBoolean(9),
                                TriggerNorthWest = reader.GetBoolean(10),
                                PreAdGroupId = reader.IsDBNull(11) == true ? (Guid?)null : reader.GetGuid(11),
                                PostAdGroupId = reader.IsDBNull(12) == true ? (Guid?)null : reader.GetGuid(12),
                                RoadType = (RoadTypes)reader.GetInt32(13),
                                RoadNumber = reader.GetInt32(14),
                                RoadState = reader.GetString(15),
                                TriggerDistance = reader.GetDouble(16)
                            };

                            trigger.PostAdURLs = this.GetURLsById(trigger.PostAdGroupId);
                            trigger.PreAdURLs = this.GetURLsById(trigger.PreAdGroupId);

                            triggers2.Add(trigger);
                        }

                        insight.Triggers = triggers2.ToArray();

                        command.Connection.Close();
                    }
                }

                response.CacheId = this.SaveCache<Insight>(insights, DataCacheTypes.Temporary);
                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.Result = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public ReadRegionsResponse ReadRegions(ReadRegionsRequest request)
        {
            ReadRegionsResponse response = new ReadRegionsResponse();

            try
            {
                List<Region> regions = new List<Region>();
                Dictionary<string, int> counts = new Dictionary<string, int>();

                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    ////string queryString =
                    ////"SELECT[RegionId]" +
                    ////",COUNT(*)" +
                    ////"FROM[dbo].[Insights] " +
                    ////"GROUP BY RegionId";

                    ////SqlCommand command = new SqlCommand(queryString, connection);

                    ////command.Connection.Open();

                    ////SqlDataReader reader = command.ExecuteReader();

                    ////while (reader.Read())
                    ////{
                    ////    counts.Add(reader.GetString(0), reader.GetInt32(1));
                    ////}

                    string queryString2 =
                    "SELECT[Id]" +
                    ",[CountryName]" +
                    ",[RegionName] " +
                    "FROM[dbo].[Regions]";

                    SqlCommand command2 = new SqlCommand(queryString2, connection);

                    command2.Connection.Open();

                    SqlDataReader reader2 = command2.ExecuteReader();

                    while (reader2.Read())
                    {
                        Region region = new Region()
                        {
                            Id = reader2.GetString(0),
                            CountryName = reader2.GetString(1),
                            RegionName = reader2.GetString(2)
                        };

                        regions.Add(region);
                    }

                    command2.Connection.Close();
                }

                response.CacheId = this.SaveCache<Region>(regions, DataCacheTypes.Temporary);
                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.Result = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public WriteAudioClipsResponse WriteAudioClips(WriteAudioClipsRequest request)
        {
            WriteAudioClipsResponse response = new WriteAudioClipsResponse();

            try
            {
                AudioClip[] clips = this.LoadCache<AudioClip>(request.CacheId);

                if (clips.Length > 0)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        foreach (AudioClip clip in clips)
                        {
                            connection.Open();

                            string queryString = "SELECT COUNT(*) from [dbo].[AudioClips] WHERE [Id] = @id";

                            SqlCommand command = new SqlCommand(queryString, connection);
                            command.Parameters.AddWithValue("@id", clip.Id);
                            int count = (int)command.ExecuteScalar();

                            connection.Close();

                            if (count == 0)
                            {
                                connection.Open();

                                string commandText = "INSERT INTO AudioClips (Id, Name, Description, Type, URL) " +
                                    "VALUES (@id, @name, @description, @type, @url)";

                                command = new SqlCommand(commandText, connection);
                                command.Parameters.AddWithValue("@id", clip.Id);
                                command.Parameters.AddWithValue("@name", clip.Name);
                                command.Parameters.AddWithValue("@description", clip.Description);
                                command.Parameters.AddWithValue("@type", clip.Type);
                                command.Parameters.AddWithValue("@url", clip.URL);
                                command.ExecuteNonQuery();

                                connection.Close();
                            }
                            else
                            {
                                connection.Open();

                                string commandText = "UPDATE AudioClips SET Name=@name, Description=@description, Type=@type, URL=@url" +
                                                     "WHERE id=@id";

                                command = new SqlCommand(commandText, connection);
                                command.Parameters.AddWithValue("@id", clip.Id);
                                command.Parameters.AddWithValue("@name", clip.Name);
                                command.Parameters.AddWithValue("@description", clip.Description);
                                command.Parameters.AddWithValue("@type", clip.Type);
                                command.Parameters.AddWithValue("@url", clip.URL);
                                command.ExecuteNonQuery();

                                connection.Close();
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.ErrorMessage = ex.Message;
                response.Result = false;
            }

            response.Result = true;

            return response;
        }

        //                response.Result = true;
        //                response.Id = request.Location.Id;
        //            }
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        response.ErrorMessage = ex.Message;
        //        response.Result = false;
        //    }
        public WriteAudioGroupLinksResponse WriteAudioGroupLinks(WriteAudioGroupLinksRequest request)
        {
            WriteAudioGroupLinksResponse response = new WriteAudioGroupLinksResponse();

            try
            {
                AudioGroupLink[] links = this.LoadCache<AudioGroupLink>(request.CacheId);

                if (links.Length > 0)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        foreach (AudioGroupLink link in links)
                        {
                            connection.Open();

                            string commandText = "INSERT INTO AudioGroupLinks (AudioGroupId, AudioClipId) " +
                                "VALUES (@audiogroupid, @audioclipid)";

                            SqlCommand command = new SqlCommand(commandText, connection);
                            command.Parameters.AddWithValue("@audiogroupid", link.AudioGroupId);
                            command.Parameters.AddWithValue("@audioclipid", link.AudioClipId);
                            command.ExecuteNonQuery();

                            connection.Close();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.ErrorMessage = ex.Message;
                response.Result = false;
            }

            response.Result = true;

            return response;
        }

        public WriteAudioGroupsResponse WriteAudioGroups(WriteAudioGroupsRequest request)
        {
            WriteAudioGroupsResponse response = new WriteAudioGroupsResponse();

            try
            {
                AudioGroup[] groups = this.LoadCache<AudioGroup>(request.CacheId);

                if (groups.Length > 0)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        foreach (AudioGroup group in groups)
                        {
                            connection.Open();

                            string queryString = "SELECT COUNT(*) from [dbo].[AudioGroups] WHERE [Id] = @id";

                            SqlCommand command = new SqlCommand(queryString, connection);
                            command.Parameters.AddWithValue("@id", group.Id);
                            int count = (int)command.ExecuteScalar();

                            connection.Close();

                            if (count == 0)
                            {
                                connection.Open();

                                string commandText = "INSERT INTO AudioGroups (Id, Name, Description, Type) " +
                                    "VALUES (@id, @name, @description, @type)";

                                command = new SqlCommand(commandText, connection);
                                command.Parameters.AddWithValue("@id", group.Id);
                                command.Parameters.AddWithValue("@name", group.Name);
                                command.Parameters.AddWithValue("@description", group.Description);
                                command.Parameters.AddWithValue("@type", group.Type);
                                command.ExecuteNonQuery();

                                connection.Close();
                            }
                            else
                            {
                                connection.Open();

                                string commandText = "UPDATE AudioGroups SET Name=@name, Description=@description, Type=@type " +
                                                     "WHERE id=@id";

                                command = new SqlCommand(commandText, connection);
                                command.Parameters.AddWithValue("@id", group.Id);
                                command.Parameters.AddWithValue("@name", group.Name);
                                command.Parameters.AddWithValue("@description", group.Description);
                                command.Parameters.AddWithValue("@type", group.Type);
                                command.ExecuteNonQuery();

                                commandText = "DELETE GroupLinks WHERE ParentGroupId=@id";
                                command = new SqlCommand(commandText, connection);
                                command.Parameters.AddWithValue("@id", group.Id);
                                command.ExecuteNonQuery();

                                commandText = "DELETE GroupLinks WHERE ChildGroupId=@id";
                                command = new SqlCommand(commandText, connection);
                                command.Parameters.AddWithValue("@id", group.Id);
                                command.ExecuteNonQuery();

                                commandText = "DELETE AudioGroupLinks WHERE AudioGroupId=@id";
                                command = new SqlCommand(commandText, connection);
                                command.Parameters.AddWithValue("@id", group.Id);
                                command.ExecuteNonQuery();

                                connection.Close();
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.ErrorMessage = ex.Message;
                response.Result = false;
            }

            response.Result = true;

            return response;
        }

        /// <summary>
        /// Writes the specified data to the cache
        /// </summary>
        /// <param name="request">The data to be written to the cache</param>
        /// <returns>A response object containing the cache id that was used</returns>
        public WriteCacheResponse WriteCache(WriteCacheRequest request)
        {
            WriteCacheResponse response = new WriteCacheResponse();

            try
            {
                if (request.Sequence == 0)
                {
                    request.CacheId = Guid.NewGuid();
                }

                if (request.ForcedGuid.HasValue == true)
                {
                    request.CacheId = request.ForcedGuid.Value;
                }

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string commandText = "INSERT INTO DataCache (Id, Sequence, Created, Data, Length, LastSegment, Type) " +
                        "VALUES (@id, @sequence, @created, @data, @length, @lastSegment, @type)";

                    SqlCommand command = new SqlCommand(commandText, connection);
                    command.Parameters.AddWithValue("@id", request.CacheId);
                    command.Parameters.AddWithValue("@sequence", request.Sequence);
                    command.Parameters.AddWithValue("@created", DateTime.UtcNow);
                    command.Parameters.AddWithValue("@data", request.Data);
                    command.Parameters.AddWithValue("@length", request.Data.Length);
                    command.Parameters.AddWithValue("@lastSegment", request.LastSegment);
                    command.Parameters.AddWithValue("@type", (int)request.Type);

                    command.ExecuteNonQuery();

                    connection.Close();
                }

                response.CacheId = request.CacheId;
                response.Result = true;
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.ErrorMessage = ex.Message;
                response.Result = false;
            }

            return response;
        }

        public WriteGroupLinksResponse WriteGroupLinks(WriteGroupLinksRequest request)
        {
            WriteGroupLinksResponse response = new WriteGroupLinksResponse();

            try
            {
                GroupLink[] links = this.LoadCache<GroupLink>(request.CacheId);

                if (links.Length > 0)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        foreach (GroupLink link in links)
                        {
                            connection.Open();

                            string commandText = "INSERT INTO GroupLinks (ParentGroupId, ChildGroupId) " +
                                "VALUES (@parentgroupid, @childgroupid)";

                            SqlCommand command = new SqlCommand(commandText, connection);
                            command.Parameters.AddWithValue("@parentgroupid", link.ParentGroupId);
                            command.Parameters.AddWithValue("@childgroupid", link.ChildGroupId);
                            command.ExecuteNonQuery();

                            connection.Close();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.ErrorMessage = ex.Message;
                response.Result = false;
            }

            response.Result = true;

            return response;
        }

        public WriteInsightsResponse WriteInsights(WriteInsightsRequest request)
        {
            WriteInsightsResponse response = new WriteInsightsResponse();

            try
            {
                Insight[] insights = this.LoadCache<Routopia.Shared.Insight>(request.CacheId);

                if (insights.Length > 0)
                {
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        foreach (Routopia.Shared.Insight insight in insights)
                        {
                            connection.Open();

                            string queryString = "SELECT COUNT(*) from [dbo].[Insights] WHERE [Id] = @id";

                            SqlCommand command = new SqlCommand(queryString, connection);
                            command.Parameters.AddWithValue("@id", insight.Id);
                            int count = (int)command.ExecuteScalar();

                            connection.Close();

                            if (count == 0)
                            {
                                connection.Open();

                                string commandText = "INSERT INTO Insights (Id, Name, Description, Longitude, Latitude, IntroGroupId, AudioId, OutroGroupId) " +
                                    "VALUES (@id, @name, @description, @longitude, @latitude, @introgroupid, @audioid, @outrogroupid)";

                                command = new SqlCommand(commandText, connection);
                                command.Parameters.AddWithValue("@id", insight.Id);
                                command.Parameters.AddWithValue("@name", insight.Name);
                                command.Parameters.AddWithValue("@description", insight.Description);
                                command.Parameters.AddWithValue("@longitude", insight.Longitude);
                                command.Parameters.AddWithValue("@latitude", insight.Latitude);
                                command.Parameters.AddWithValue("@introgroupid", (object)insight.IntroGroupId ?? DBNull.Value);
                                command.Parameters.AddWithValue("@audioid", (object)insight.AudioId ?? DBNull.Value);
                                command.Parameters.AddWithValue("@outrogroupid", (object)insight.OutroGroupId ?? DBNull.Value);
                                command.ExecuteNonQuery();

                                foreach (Trigger trigger in insight.Triggers)
                                {
                                    commandText = "INSERT INTO Triggers (Id, Latitude, Longitude, TriggerNorth, TriggerNorthEast, TriggerEast, TriggerSouthEast, TriggerSouth, TriggerSouthWest, TriggerWest, TriggerNorthWest, InsightId, PreAdGroupId, PostAdGroupId, RoadType, RoadNumber, RoadState, TriggerDistance) " +
                                        "VALUES (@id, @latitude, @longitude, @triggernorth, @triggernortheast, @triggereast, @triggersoutheast, @triggersouth, @triggersouthwest, @triggerwest, @triggernorthwest, @insightid, @preadgroupid, @postadgroupid, @roadtype, @roadnumber, @roadstate, @triggerdistance)";

                                    command = new SqlCommand(commandText, connection);
                                    command.Parameters.AddWithValue("@id", Guid.NewGuid());
                                    command.Parameters.AddWithValue("@latitude", trigger.Latitude);
                                    command.Parameters.AddWithValue("@longitude", trigger.Longitude);
                                    command.Parameters.AddWithValue("@triggernorth", trigger.TriggerNorth);
                                    command.Parameters.AddWithValue("@triggernortheast", trigger.TriggerNorthEast);
                                    command.Parameters.AddWithValue("@triggereast", trigger.TriggerEast);
                                    command.Parameters.AddWithValue("@triggersoutheast", trigger.TriggerSouthEast);
                                    command.Parameters.AddWithValue("@triggersouth", trigger.TriggerSouth);
                                    command.Parameters.AddWithValue("@triggersouthwest", trigger.TriggerSouthWest);
                                    command.Parameters.AddWithValue("@triggerwest", trigger.TriggerWest);
                                    command.Parameters.AddWithValue("@triggernorthwest", trigger.TriggerNorthWest);
                                    command.Parameters.AddWithValue("@insightid", insight.Id);
                                    command.Parameters.AddWithValue("@preadgroupid", (object)trigger.PreAdGroupId ?? DBNull.Value);
                                    command.Parameters.AddWithValue("@postadgroupid", (object)trigger.PostAdGroupId ?? DBNull.Value);
                                    command.Parameters.AddWithValue("@roadtype", trigger.RoadType);
                                    command.Parameters.AddWithValue("@roadnumber", trigger.RoadNumber);
                                    command.Parameters.AddWithValue("@roadstate", trigger.RoadState);
                                    command.Parameters.AddWithValue("@triggerdistance", trigger.TriggerDistance);
                                    command.ExecuteNonQuery();
                                }

                                connection.Close();
                            }
                            else
                            {
                                connection.Open();

                                // Update the locations
                                string commandText = "UPDATE Insights SET Name=@name,Description=@description,Longitude=@longitude,Latitude=@latitude," +
                                                     "IntroGroupId=@introgroupid, AudioId=@audioid, OutroGroupId=@outrogroupid " +
                                                     "WHERE Id=@id";

                                command = new SqlCommand(commandText, connection);
                                command.Parameters.AddWithValue("@id", insight.Id);
                                command.Parameters.AddWithValue("@name", insight.Name);
                                command.Parameters.AddWithValue("@description", insight.Description);
                                command.Parameters.AddWithValue("@longitude", insight.Longitude);
                                command.Parameters.AddWithValue("@latitude", insight.Latitude);
                                command.Parameters.AddWithValue("@introgroupid", (object)insight.IntroGroupId ?? DBNull.Value);
                                command.Parameters.AddWithValue("@audioid", (object)insight.AudioId ?? DBNull.Value);
                                command.Parameters.AddWithValue("@outrogroupid", (object)insight.OutroGroupId ?? DBNull.Value);
                                command.ExecuteNonQuery();

                                // Delete out the triggers
                                commandText = "DELETE Triggers WHERE InsightId=@insightid";
                                command = new SqlCommand(commandText, connection);
                                command.Parameters.AddWithValue("@insightid", insight.Id);
                                command.ExecuteNonQuery();

                                // Add the new zone points
                                foreach (Trigger trigger in insight.Triggers)
                                {
                                    commandText = "INSERT INTO Triggers (Id, Latitude, Longitude, TriggerNorth, TriggerNorthEast, TriggerEast, TriggerSouthEast, TriggerSouth, TriggerSouthWest, TriggerWest, TriggerNorthWest, InsightId, PreAdGroupId, PostAdGroupId, RoadType, RoadNumber, RoadState, TriggerDistance) " +
                                        "VALUES (@id, @latitude, @longitude, @triggernorth, @triggernortheast, @triggereast, @triggersoutheast, @triggersouth, @triggersouthwest, @triggerwest, @triggernorthwest, @insightid, @preadgroupid, @postadgroupid, @roadtype, @roadnumber, @roadstate, @triggerdistance)";

                                    command = new SqlCommand(commandText, connection);
                                    command.Parameters.AddWithValue("@id", Guid.NewGuid());
                                    command.Parameters.AddWithValue("@latitude", trigger.Latitude);
                                    command.Parameters.AddWithValue("@longitude", trigger.Longitude);
                                    command.Parameters.AddWithValue("@triggernorth", trigger.TriggerNorth);
                                    command.Parameters.AddWithValue("@triggernortheast", trigger.TriggerNorthEast);
                                    command.Parameters.AddWithValue("@triggereast", trigger.TriggerEast);
                                    command.Parameters.AddWithValue("@triggersoutheast", trigger.TriggerSouthEast);
                                    command.Parameters.AddWithValue("@triggersouth", trigger.TriggerSouth);
                                    command.Parameters.AddWithValue("@triggersouthWest", trigger.TriggerSouthWest);
                                    command.Parameters.AddWithValue("@triggerwest", trigger.TriggerWest);
                                    command.Parameters.AddWithValue("@triggernorthwest", trigger.TriggerNorthWest);
                                    command.Parameters.AddWithValue("@insightid", insight.Id);
                                    command.Parameters.AddWithValue("@preadgroupid", (object)trigger.PreAdGroupId ?? DBNull.Value);
                                    command.Parameters.AddWithValue("@postadgroupid", (object)trigger.PostAdGroupId ?? DBNull.Value);
                                    command.Parameters.AddWithValue("@roadtype", trigger.RoadType);
                                    command.Parameters.AddWithValue("@roadnumber", trigger.RoadNumber);
                                    command.Parameters.AddWithValue("@roadstate", trigger.RoadState);
                                    command.Parameters.AddWithValue("@triggerdistance", trigger.TriggerDistance);
                                    command.ExecuteNonQuery();
                                }

                                connection.Close();
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                this.Log(ex.Message);
                response.ErrorMessage = ex.Message;
                response.Result = false;
            }

            response.Result = true;

            return response;
        }

        /// <summary>
        /// Deserializes a list from a cache object
        /// </summary>
        /// <typeparam name="T">The type of object contained in the list</typeparam>
        /// <param name="cacheId">The id of the cache to deserialize</param>
        /// <returns>A list containing the specified object types</returns>
        private T[] LoadCache<T>(Guid cacheId)
        {
            T[] result = null;

            try
            {
                using (SqlConnection connection = new SqlConnection(
                           connectionString))
                {
                    string queryString =
                    "SELECT[Id]" +
                    ",[Sequence]" +
                    ",[Data]" +
                    ",[Length]" +
                    "FROM[dbo].[DataCache] " +
                    "WHERE [Id]=@id ORDER BY [Sequence]";

                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@id", cacheId);

                    command.Connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    List<byte> data = new List<byte>();

                    while (reader.Read())
                    {
                        Guid id = reader.GetGuid(0);
                        int sequence = reader.GetInt32(1);
                        int length = reader.GetInt32(3);

                        byte[] buffer = new byte[length];

                        reader.GetBytes(2, 0, buffer, 0, buffer.Length);

                        data.AddRange(buffer);
                    }

                    command.Connection.Close();

                    using (MemoryStream stream = new MemoryStream(data.ToArray()))
                    {
                        DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T[]));

                        result = serializer.ReadObject(stream) as T[];
                    }
                }

                DeleteCacheRequest request = new DeleteCacheRequest()
                {
                    CacheId = cacheId
                };

                this.DeleteCache(request);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

            return result;
        }

        /// <summary>
        /// Serializes the specified list and returns the cache id
        /// </summary>
        /// <typeparam name="T">The type of object in the list</typeparam>
        /// <param name="list">The list to serialize</param>
        /// <returns>A response object containing the cache id</returns>
        private Guid SaveCache<T>(List<T> list, DataCacheTypes type)
        {
            byte[] data = null;

            using (MemoryStream stream = new MemoryStream())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T[]));

                serializer.WriteObject(stream, list.ToArray());
                data = stream.ToArray();
            }

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                Guid cacheId = Guid.NewGuid();

                connection.Open();

                int size = data.Length;
                int offset = 0;
                int sequence = 0;

                while (size > 0)
                {
                    if (size > 32 * 1024)
                    {
                        byte[] buffer = new byte[32 * 1024];

                        Array.Copy(data, offset, buffer, 0, buffer.Length);

                        string commandText = "INSERT INTO DataCache (Id, Sequence, Created, Data, Length, LastSegment, Type) " +
                            "VALUES (@id, @sequence, @created, @data, @length, @lastSegment, @type)";

                        SqlCommand command = new SqlCommand(commandText, connection);
                        command.Parameters.AddWithValue("@id", cacheId);
                        command.Parameters.AddWithValue("@sequence", sequence);
                        command.Parameters.AddWithValue("@created", DateTime.UtcNow);
                        command.Parameters.AddWithValue("@data", buffer);
                        command.Parameters.AddWithValue("@length", buffer.Length);
                        command.Parameters.AddWithValue("@lastSegment", false);
                        command.Parameters.AddWithValue("@type", (int)type);
                        command.ExecuteNonQuery();

                        size -= 32 * 1024;
                        offset += 32 * 1024;
                    }
                    else
                    {
                        byte[] buffer = new byte[size];

                        Array.Copy(data, offset, buffer, 0, buffer.Length);

                        string commandText = "INSERT INTO DataCache (Id, Sequence, Created, Data, Length, LastSegment, Type) " +
                            "VALUES (@id, @sequence, @created, @data, @length, @lastSegment, @type)";

                        SqlCommand command = new SqlCommand(commandText, connection);
                        command.Parameters.AddWithValue("@id", cacheId);
                        command.Parameters.AddWithValue("@sequence", sequence);
                        command.Parameters.AddWithValue("@created", DateTime.UtcNow);
                        command.Parameters.AddWithValue("@data", buffer);
                        command.Parameters.AddWithValue("@length", buffer.Length);
                        command.Parameters.AddWithValue("@lastSegment", true);
                        command.Parameters.AddWithValue("@type", (int)type);
                        command.ExecuteNonQuery();

                        size = 0;
                    }

                    sequence++;
                }

                connection.Close();

                return cacheId;
            }
        }

        /// <summary>
        /// Reads the audio group links
        /// </summary>
        /// <param name="request">The request object containing the link id</param>
        /// <returns></returns>

        //public AudioClipResponse SaveAudioClip(AudioClipRequest request)
        //{
        //    AudioClipResponse response = new AudioClipResponse();

        //    try
        //    {
        //        string connectionString = @"Server=tcp:almosttendbserver.database.windows.net,1433;Initial Catalog=BlueCopper;Persist Security Info=False;User ID=mattaolsen;Password=Id0ntknow;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        //        using (SqlConnection connection = new SqlConnection(connectionString))
        //        {
        //            if (request.Clip.Id == Guid.Empty)
        //            {
        //                Guid newGuid = Guid.NewGuid();

        //                connection.Open();

        //                string commandText = "INSERT INTO AudioClips (Id, Name, Description, Data, Type, LastUpdated, DataLength, Created) " +
        //                    "VALUES (@id, @name, @description, @data, @type, @lastupdated, @datalength, @created)";

        //                SqlCommand command = new SqlCommand(commandText, connection);
        //                command.Parameters.AddWithValue("@id", newGuid);
        //                command.Parameters.AddWithValue("@name", request.Clip.Name);
        //                command.Parameters.AddWithValue("@description", request.Clip.Description);
        //                command.Parameters.AddWithValue("@data", request.Clip.Data);
        //                command.Parameters.AddWithValue("@type", request.Clip.Type);
        //                command.Parameters.AddWithValue("@lastupdated", request.Clip.LastUpdated);
        //                command.Parameters.AddWithValue("@datalength", request.Clip.DataLength);
        //                command.Parameters.AddWithValue("@created", DateTime.UtcNow);
        //                command.ExecuteNonQuery();

        //                connection.Close();

        //                response.Result = true;
        //                response.Clip = request.Clip;
        //                response.Clip.Id = newGuid;
        //            }
        //            else
        //            {
        //                ////connection.Open();

        //                ////// Update the locations
        //                ////string commandText = "UPDATE Locations SET Name=@name,Description=@description,LatitudeNorth=@latitudenorth,LatitudeSouth=@latitudesouth," +
        //                ////                     "LongitudeWest=@longitudeWest,LongitudeEast=@longitudeeast,LastUpdated=@lastupdated " +
        //                ////                     "WHERE Id=@id";

        //                ////SqlCommand command = new SqlCommand(commandText, connection);
        //                ////command.Parameters.AddWithValue("@id", request.Location.Id);
        //                ////command.Parameters.AddWithValue("@name", request.Location.Name);
        //                ////command.Parameters.AddWithValue("@description", request.Location.Description);
        //                ////command.Parameters.AddWithValue("@latitudenorth", request.Location.LatitudeNorth);
        //                ////command.Parameters.AddWithValue("@latitudesouth", request.Location.LatitudeSouth);
        //                ////command.Parameters.AddWithValue("@longitudewest", request.Location.LongitudeWest);
        //                ////command.Parameters.AddWithValue("@longitudeeast", request.Location.LongitudeEast);
        //                ////command.Parameters.AddWithValue("@lastupdated", request.Location.LastUpdated);
        //                ////command.ExecuteNonQuery();

        //                ////// Delete out the zone points
        //                ////commandText = "DELETE ZonePoints WHERE LocationId=@locationid";
        //                ////command = new SqlCommand(commandText, connection);
        //                ////command.Parameters.AddWithValue("@locationid", request.Location.Id);
        //                ////command.ExecuteNonQuery();

        //                ////// Add the new zone points
        //                ////foreach (ZonePoint zonePoint in request.Location.ZonePoints)
        //                ////{
        //                ////    commandText = "INSERT INTO ZonePoints (Latitude, Longitude, LocationId) " +
        //                ////        "VALUES (@latitude, @longitude, @locationid)";
        //                ////    command = new SqlCommand(commandText, connection);
        //                ////    command.Parameters.AddWithValue("@latitude", zonePoint.Latitude);
        //                ////    command.Parameters.AddWithValue("@longitude", zonePoint.Longitude);
        //                ////    command.Parameters.AddWithValue("@locationid", request.Location.Id);
        //                ////    command.ExecuteNonQuery();
        //                ////}

        //                ////connection.Close();

        //                ////response.Result = true;
        //                ////response.Location = request.Location;
        //            }
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        response.ErrorMessage = ex.Message;
        //        response.Result = false;
        //    }

        //    return response;
        //}

        //public LocationResponse SaveLocation(LocationRequest request)
        //{
        //    LocationResponse response = new LocationResponse();

        //    try
        //    {
        //        string connectionString = @"Server=tcp:almosttendbserver.database.windows.net,1433;Initial Catalog=BlueCopper;Persist Security Info=False;User ID=mattaolsen;Password=Id0ntknow;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        //        using (SqlConnection connection = new SqlConnection(connectionString))
        //        {
        //            if (request.Location.Id == Guid.Empty)
        //            {
        //                Guid newGuid = Guid.NewGuid();

        //                connection.Open();

        //                string commandText = "INSERT INTO Locations (Id, Name, Description, LatitudeNorth, LatitudeSouth, LongitudeWest, LongitudeEast, LastUpdated, Created) " +
        //                    "VALUES (@id, @name, @description, @latitudenorth, @latitudesouth, @longitudewest, @longitudeeast, @lastupdated, @created)";

        //                SqlCommand command = new SqlCommand(commandText, connection);
        //                command.Parameters.AddWithValue("@id", newGuid);
        //                command.Parameters.AddWithValue("@name", request.Location.Name);
        //                command.Parameters.AddWithValue("@description", request.Location.Description);
        //                command.Parameters.AddWithValue("@latitudenorth", request.Location.LatitudeNorth);
        //                command.Parameters.AddWithValue("@latitudesouth", request.Location.LatitudeSouth);
        //                command.Parameters.AddWithValue("@longitudewest", request.Location.LongitudeWest);
        //                command.Parameters.AddWithValue("@longitudeeast", request.Location.LongitudeEast);
        //                command.Parameters.AddWithValue("@lastupdated", request.Location.LastUpdated);
        //                command.Parameters.AddWithValue("@created", DateTime.UtcNow);
        //                command.ExecuteNonQuery();

        //                foreach (ZonePoint zonePoint in request.Location.ZonePoints)
        //                {
        //                    commandText = "INSERT INTO ZonePoints (Latitude, Longitude, LocationId) " +
        //                        "VALUES (@latitude, @longitude, @locationid)";
        //                    command = new SqlCommand(commandText, connection);
        //                    command.Parameters.AddWithValue("@latitude", zonePoint.Latitude);
        //                    command.Parameters.AddWithValue("@longitude", zonePoint.Longitude);
        //                    command.Parameters.AddWithValue("@locationid", newGuid);
        //                    command.ExecuteNonQuery();
        //                }

        //                connection.Close();

        //                response.Result = true;
        //                response.Id = newGuid;
        //            }
        //            else
        //            {
        //                connection.Open();

        //                // Update the locations
        //                string commandText = "UPDATE Locations SET Name=@name,Description=@description,LatitudeNorth=@latitudenorth,LatitudeSouth=@latitudesouth," +
        //                                     "LongitudeWest=@longitudeWest,LongitudeEast=@longitudeeast,LastUpdated=@lastupdated " +
        //                                     "WHERE Id=@id";

        //                SqlCommand command = new SqlCommand(commandText, connection);
        //                command.Parameters.AddWithValue("@id", request.Location.Id);
        //                command.Parameters.AddWithValue("@name", request.Location.Name);
        //                command.Parameters.AddWithValue("@description", request.Location.Description);
        //                command.Parameters.AddWithValue("@latitudenorth", request.Location.LatitudeNorth);
        //                command.Parameters.AddWithValue("@latitudesouth", request.Location.LatitudeSouth);
        //                command.Parameters.AddWithValue("@longitudewest", request.Location.LongitudeWest);
        //                command.Parameters.AddWithValue("@longitudeeast", request.Location.LongitudeEast);
        //                command.Parameters.AddWithValue("@lastupdated", request.Location.LastUpdated);
        //                command.ExecuteNonQuery();

        //                // Delete out the zone points
        //                commandText = "DELETE ZonePoints WHERE LocationId=@locationid";
        //                command = new SqlCommand(commandText, connection);
        //                command.Parameters.AddWithValue("@locationid", request.Location.Id);
        //                command.ExecuteNonQuery();
    }
}