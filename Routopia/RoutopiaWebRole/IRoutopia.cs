﻿using Routopia.Shared;
using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace RoutopiaWebRole
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IRoutopiaService
    {
        [OperationContract]
        AttachAudioResponse AttachAudio(AttachAudioRequest request);

        //[OperationContract]
        //AttachGroupsResponse AttachGroups(AttachGroupsRequest request);

        [OperationContract]
        DeleteAudioClipsResponse DeleteAudioClips(DeleteAudioClipsRequest request);

        [OperationContract]
        DeleteAudioGroupLinkResponse DeleteAudioGroupLink(DeleteAudioGroupLinkRequest request);

        [OperationContract]
        DeleteAudioGroupResponse DeleteAudioGroups(DeleteAudioGroupRequest request);

        [OperationContract]
        DeleteCacheResponse DeleteCache(DeleteCacheRequest request);

        [OperationContract]
        DeleteGroupLinksResponse DeleteGroupLinks(DeleteGroupLinksRequest request);

        [OperationContract]
        DeleteInsightResponse DeleteInsight(DeleteInsightRequest request);

        [OperationContract]
        DetachAudioResponse DetachAudio(DetachAudioRequest request);

        [OperationContract]
        DetachGroupResponse DetachGroup(DetachGroupRequest request);

        [OperationContract]
        ReadAudioClipsResponse ReadAudioClips(ReadAudioClipsRequest request);

        [OperationContract]
        ReadAudioGroupLinksResponse ReadAudioGroupLinks(ReadAudioGroupLinksRequest request);

        [OperationContract]
        ReadAudioGroupsResponse ReadAudioGroups(ReadAudioGroupsRequest request);

        [OperationContract]
        ReadAudioTypesResponse ReadAudioTypes(ReadAudioTypesRequest request);

        [OperationContract]
        ReadCacheResponse ReadCache(ReadCacheRequest request);

        [OperationContract]
        ReadDeletionHistoryResponse ReadDeletionHistory(ReadDeletionHistoryRequest request);

        [OperationContract]
        ReadGroupLinksResponse ReadGroupLinks(ReadGroupLinksRequest request);

        [OperationContract]
        ReadInsightsResponse ReadInsights(ReadInsightsRequest request);

        [OperationContract]
        ReadInsightsByLocationResponse ReadInsightsByLocation(ReadInsightsByLocationRequest request);

        [OperationContract]
        ReadInsightsByRegionResponse ReadInsightsByRegion(ReadInsightsByRegionRequest request);

        [OperationContract]
        ReadRegionsResponse ReadRegions(ReadRegionsRequest request);

        [OperationContract]
        WriteAudioClipsResponse WriteAudioClips(WriteAudioClipsRequest request);

        [OperationContract]
        WriteAudioGroupLinksResponse WriteAudioGroupLinks(WriteAudioGroupLinksRequest request);

        [OperationContract]
        WriteAudioGroupsResponse WriteAudioGroups(WriteAudioGroupsRequest request);

        [OperationContract]
        WriteCacheResponse WriteCache(WriteCacheRequest request);

        [OperationContract]
        WriteGroupLinksResponse WriteGroupLinks(WriteGroupLinksRequest request);

        [OperationContract]
        WriteInsightsResponse WriteInsights(WriteInsightsRequest request);
    }

    [DataContract]
    public class AttachAudioRequest : RequestBase
    {
        [DataMember]
        public Guid AudioGroupId
        {
            get;
            set;
        }

        [DataMember]
        public Guid AudioId
        {
            get;
            set;
        }
    }

    [DataContract]
    public class AttachAudioResponse : ResponseBase
    {
    }

    //[DataContract]
    //public class AttachGroupsRequest : RequestBase
    //{
    //}

    //[DataContract]
    //public class AttachGroupsResponse : ResponseBase
    //{
    //}

    [DataContract]
    public class DeleteAudioClipsRequest : RequestBase
    {
        [DataMember]
        public Guid[] AudioClipIdList
        {
            get;
            set;
        }
    }

    [DataContract]
    public class DeleteAudioClipsResponse : ResponseBase
    {
    }

    [DataContract]
    public class DeleteAudioGroupLinkRequest : RequestBase
    {
    }

    [DataContract]
    public class DeleteAudioGroupLinkResponse : ResponseBase
    {
        [DataMember]
        public Guid AudioGroupLinkId
        {
            get;
            set;
        }
    }

    [DataContract]
    public class DeleteAudioGroupRequest : RequestBase
    {
        [DataMember]
        public Guid[] AudioGroupIdList
        {
            get;
            set;
        }
    }

    [DataContract]
    public class DeleteAudioGroupResponse : ResponseBase
    {
    }

    [DataContract]
    public class DeleteCacheRequest : RequestBase
    {
    }

    [DataContract]
    public class DeleteCacheResponse : ResponseBase
    {
    }

    [DataContract]
    public class DeleteGroupLinksRequest : RequestBase
    {
    }

    [DataContract]
    public class DeleteGroupLinksResponse : ResponseBase
    {
    }

    [DataContract]
    public class DeleteInsightRequest : RequestBase
    {
        [DataMember]
        public Guid InsightId
        {
            get;
            set;
        }
    }

    [DataContract]
    public class DeleteInsightResponse : ResponseBase
    {
    }

    [DataContract]
    public class DetachAudioRequest : RequestBase
    {
        [DataMember]
        public Guid AudioGroupId
        {
            get;
            set;
        }

        [DataMember]
        public Guid AudioId
        {
            get;
            set;
        }
    }

    [DataContract]
    public class DetachAudioResponse : ResponseBase
    {
    }

    [DataContract]
    public class DetachGroupRequest : RequestBase
    {
        public Guid AudioGroupChild
        {
            get;
            set;
        }

        public Guid AudioGroupParent
        {
            get;
            set;
        }
    }

    [DataContract]
    public class DetachGroupResponse : ResponseBase
    {
    }

    [DataContract]
    public class LocationRequest : RequestBase
    {
        public LocationRequest(Insight location)
        {
            this.Location = location;
        }

        [DataMember]
        public Insight Location
        {
            get;
            set;
        }
    }

    [DataContract]
    public class ReadAudioClipsRequest : RequestBase
    {
    }

    [DataContract]
    public class ReadAudioClipsResponse : ResponseBase
    {
    }

    [DataContract]
    public class ReadAudioGroupLinksRequest : RequestBase
    {
    }

    [DataContract]
    public class ReadAudioGroupLinksResponse : ResponseBase
    {
    }

    [DataContract]
    public class ReadAudioGroupsRequest : RequestBase
    {
    }

    [DataContract]
    public class ReadAudioGroupsResponse : ResponseBase
    {
    }

    [DataContract]
    public class ReadAudioTypesRequest : RequestBase
    {
    }

    [DataContract]
    public class ReadAudioTypesResponse : ResponseBase
    {
    }

    [DataContract]
    public class ReadCacheRequest : RequestBase
    {
        public ReadCacheRequest(Guid cacheId, int sequence)
        {
            this.CacheId = cacheId;
            this.Sequence = sequence;
        }

        /// <summary>
        /// Gets or sets the sequence of the cached data
        /// </summary>
        [DataMember]
        public int Sequence
        {
            get;
            set;
        }
    }

    [DataContract]
    public class ReadCacheResponse : ResponseBase
    {
        /// <summary>
        /// Gets or sets a value indicating whether or not the cache read is complete
        /// </summary>
        [DataMember]
        public bool Complete
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the data
        /// </summary>
        [DataMember]
        public byte[] Data
        {
            get;
            set;
        }
    }

    [DataContract]
    public class ReadDeletionHistoryRequest : RequestBase
    {
        [DataMember]
        public DateTime? LastUpdate
        {
            get;
            set;
        }
    }

    [DataContract]
    public class ReadDeletionHistoryResponse : ResponseBase
    {
    }

    [DataContract]
    public class ReadGroupLinksRequest : RequestBase
    {
    }

    [DataContract]
    public class ReadGroupLinksResponse : ResponseBase
    {
    }

    [DataContract]
    public class ReadInsightsByLocationRequest : RequestBase
    {
        [DataMember]
        public double DistanceInMiles
        {
            get;
            set;
        }

        [DataMember]
        public double Latitude
        {
            get;
            set;
        }

        [DataMember]
        public double Longitude
        {
            get;
            set;
        }
    }

    [DataContract]
    public class ReadInsightsByLocationResponse : ResponseBase
    {
    }

    [DataContract]
    public class ReadInsightsByRegionRequest : RequestBase
    {
        [DataMember]
        public string[] Regions
        {
            get;
            set;
        }
    }

    [DataContract]
    public class ReadInsightsByRegionResponse : ResponseBase
    {
    }

    [DataContract]
    public class ReadInsightsRequest : RequestBase
    {
    }

    [DataContract]
    public class ReadInsightsResponse : ResponseBase
    {
    }

    [DataContract]
    public class ReadRegionsRequest : RequestBase
    {
    }

    [DataContract]
    public class ReadRegionsResponse : ResponseBase
    {
    }

    [DataContract]
    public abstract class RequestBase
    {
        public RequestBase()
        {
            this.CacheId = Guid.Empty;
        }

        [DataMember]
        public Guid CacheId
        {
            get;
            set;
        }
    }

    [DataContract]
    public abstract class ResponseBase
    {
        [DataMember]
        public Guid CacheId
        {
            get;
            set;
        }

        [DataMember]
        public string ErrorMessage
        {
            get;
            set;
        }

        [DataMember]
        public bool Result
        {
            get;
            set;
        }
    }

    [DataContract]
    public class WriteAudioClipsRequest : RequestBase
    {
    }

    [DataContract]
    public class WriteAudioClipsResponse : ResponseBase
    {
    }

    [DataContract]
    public class WriteAudioGroupLinksRequest : RequestBase
    {
    }

    [DataContract]
    public class WriteAudioGroupLinksResponse : ResponseBase
    {
    }

    [DataContract]
    public class WriteAudioGroupsRequest : RequestBase
    {
    }

    [DataContract]
    public class WriteAudioGroupsResponse : ResponseBase
    {
    }

    [DataContract]
    public class WriteCacheRequest : RequestBase
    {
        [DataMember]
        public byte[] Data
        {
            get;
            set;
        }

        [DataMember]
        public Guid? ForcedGuid
        {
            get;
            set;
        }

        [DataMember]
        public bool LastSegment
        {
            get;
            set;
        }

        [DataMember]
        public int Sequence
        {
            get;
            set;
        }

        [DataMember]
        public DataCacheTypes Type
        {
            get;
            set;
        }
    }

    [DataContract]
    public class WriteCacheResponse : ResponseBase
    {
    }

    [DataContract]
    public class WriteGroupLinksRequest : RequestBase
    {
    }

    [DataContract]
    public class WriteGroupLinksResponse : ResponseBase
    {
    }

    [DataContract]
    public class WriteInsightsRequest : RequestBase
    {
    }

    [DataContract]
    public class WriteInsightsResponse : ResponseBase
    {
    }
}