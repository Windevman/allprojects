﻿using System;

namespace Routopia.Shared
{
    [Serializable]
    public class AudioGroup
    {
        public string Description { get; set; } = string.Empty;

        public Guid Id { get; set; } = Guid.Empty;

        public string Name { get; set; } = string.Empty;

        public int Type { get; set; } = 0;
    }
}