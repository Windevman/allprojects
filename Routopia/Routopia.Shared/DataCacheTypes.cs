﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Routopia.Shared
{
    public enum DataCacheTypes
    {
        Unknown = 0,
        Temporary = 1,
        AudioData = 2
    }
}
