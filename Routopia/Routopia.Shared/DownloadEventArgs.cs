﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Routopia.Shared
{
    public class DownloadEventArgs : EventArgs
    {
        public bool FileSaved = false;
        public string FilePath = string.Empty;

        public DownloadEventArgs(bool fileSaved, string filePath)
        {
            this.FileSaved = fileSaved;
            this.FilePath = filePath;
        }
    }
}
