﻿namespace Routopia.Shared
{
    using System;

    [Serializable]
    public class Insight
    {
        public Insight()
        {
            this.Id = Guid.NewGuid();
            this.Triggers = new Trigger[] { };
        }

        public Guid? AudioId { get; set; } = null;

        public string[] AudioURLs { get; set; } = null;

        public DateTime Created { get; set; }

        public string Description { get; set; } = string.Empty;

        public Guid Id { get; set; } = Guid.Empty;

        public Guid? IntroGroupId { get; set; } = null;

        public string[] IntroURLs { get; set; } = null;

        public DateTime LastUpdated { get; set; }

        public double Latitude { get; set; } = 0d;

        public double Longitude { get; set; } = 0d;

        public string Name { get; set; } = string.Empty;

        public Guid? OutroGroupId { get; set; } = null;

        public string[] OutroURLs { get; set; } = null;

        public Trigger[] Triggers { get; set; } = null;

        public string RegionId { get; set; } = null;
    }
}