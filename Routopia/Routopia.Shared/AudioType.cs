﻿using System;

namespace Routopia.Shared
{
    [Serializable]
    public class AudioType
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}