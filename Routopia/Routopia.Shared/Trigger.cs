﻿namespace Routopia.Shared
{
    using System;

    [Serializable]
    public class Trigger
    {
        public Guid Id { get; set; } = Guid.Empty;

        public Guid? InsightId { get; set; } = Guid.Empty;

        public double Latitude { get; set; } = 0d;

        public double Longitude { get; set; } = 0d;

        public Guid? PostAdGroupId { get; set; } = null;

        public string[] PostAdURLs { get; set; } = null;
        public Guid? PreAdGroupId { get; set; } = null;

        public string[] PreAdURLs { get; set; } = null;
        public int RoadNumber { get; set; } = 0;

        public string RoadState { get; set; } = string.Empty;

        public RoadTypes RoadType { get; set; } = RoadTypes.Unknown;

        public double TriggerDistance { get; set; } = 0d;

        public bool TriggerEast { get; set; } = false;

        public bool TriggerNorth { get; set; } = false;

        public bool TriggerNorthEast { get; set; } = false;

        public bool TriggerNorthWest { get; set; } = false;

        public bool TriggerSouth { get; set; } = false;

        public bool TriggerSouthEast { get; set; } = false;

        public bool TriggerSouthWest { get; set; } = false;

        public bool TriggerWest { get; set; } = false;
    }
}