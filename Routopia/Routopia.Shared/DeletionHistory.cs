﻿using System;

namespace Routopia.Shared
{
    public class DeletionHistory
    {
        public DateTime DeletionDate { get; set; } = DateTime.MinValue;

        public int Id { get; set; } = 0;

        public Guid PrimaryId { get; set; } = Guid.Empty;

        public Guid SecondaryId { get; set; } = Guid.Empty;

        public DeleteHistoryTypes Type { get; set; } = DeleteHistoryTypes.Unknown;

        public DeleteHistoryTypes Type1 { get => Type; set => Type = value; }
    }
}