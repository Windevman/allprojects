﻿using System;

namespace Routopia.Shared
{
    [Serializable]
    public class AudioGroupLink
    {
        public Guid AudioGroupId { get; set; } = Guid.Empty;

        public Guid AudioClipId { get; set; } = Guid.Empty;
    }
}