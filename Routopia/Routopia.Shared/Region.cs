﻿using System;

namespace Routopia.Shared
{
    [Serializable]
    public class Region
    {
        public string Id { get; set; } = string.Empty;
        public string CountryName { get; set; } = string.Empty;
        public string RegionName { get; set; } = string.Empty;
    }
}