﻿using System;

namespace Routopia.Shared
{
    public class SoundCollection
    {
        public SoundCollection()
        {
            CurrentStep = Steps.Intro;
        }

        public enum Steps
        {
            Intro = 0,
            PreAd = 1,
            Audio = 2,
            PostAd = 3,
            Outro = 4,
            Complete = 5
        }

        public string[] AudioUrls
        {
            get;
            set;
        }

        public string[] BackgroundUrls
        {
            get;
            set;
        }

        public Steps CurrentStep
        {
            get;
            set;
        }

        public string[] IntroUrls
        {
            get;
            set;
        }

        public string[] OutroUrls
        {
            get;
            set;
        }

        public string[] PostAdUrls
        {
            get;
            set;
        }

        public string[] PreAdUrls
        {
            get;
            set;
        }

        public object Tag
        {
            get;
            set;
        }

        public string GetURL()
        {
            string[] listToUse = null;

            while (listToUse == null && this.CurrentStep != Steps.Complete)
            {
                switch (this.CurrentStep)
                {
                    case Steps.Intro:
                        listToUse = this.IntroUrls;
                        break;

                    case Steps.PreAd:
                        listToUse = this.PreAdUrls;
                        break;

                    case Steps.Audio:
                        listToUse = this.AudioUrls;
                        break;

                    case Steps.PostAd:
                        listToUse = this.PostAdUrls;
                        break;

                    case Steps.Outro:
                        listToUse = this.OutroUrls;
                        break;
                }

                if (listToUse == null)
                {
                    this.CurrentStep++;
                }
            }

            Random random = new Random();

            if (listToUse != null && listToUse.Length > 0)
            {
                return listToUse[random.Next(0, listToUse.Length - 1)];
            }

            return null;
        }

        public void NextStep()
        {
            bool validStep = false;

            while (validStep == false)
            {
                if (CurrentStep < Steps.Complete)
                {
                    CurrentStep++;

                    switch (this.CurrentStep)
                    {
                        case Steps.Intro:
                            if (this.IntroUrls != null && this.IntroUrls.Length > 0)
                            {
                                validStep = true;
                            }
                            break;

                        case Steps.PreAd:
                            if (this.PreAdUrls != null && this.PreAdUrls.Length > 0)
                            {
                                validStep = true;
                            }
                            break;

                        case Steps.Audio:
                            if (this.AudioUrls != null && this.AudioUrls.Length > 0)
                            {
                                validStep = true;
                            }

                            break;

                        case Steps.PostAd:
                            if (this.PostAdUrls != null && this.PostAdUrls.Length > 0)
                            {
                                validStep = true;
                            }

                            break;

                        case Steps.Outro:
                            if (this.OutroUrls != null && this.OutroUrls.Length > 0)
                            {
                                validStep = true;
                            }

                            break;
                    }
                }
                else
                {
                    validStep = true;
                }
            }
        }
    }
}