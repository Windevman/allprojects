﻿using System;

namespace Routopia.Shared
{
    [Serializable]
    public class GroupLink
    {
        public Guid ParentGroupId { get; set; } = Guid.Empty;

        public Guid ChildGroupId { get; set; } = Guid.Empty;
    }
}