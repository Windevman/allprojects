﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Routopia.Shared
{
    public enum DeleteHistoryTypes
    {
        Unknown = 0,
        Insight = 1,
        AudioGroups = 2,
        AudioGroupLinks = 3,
        AudioClips = 4,
        GroupLinks = 5
    }
}
